﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
 * Extensions for Tilemap.
 */
public static class TilemapExtensions
{
    /**
     * Returns whether the specified cell has a tile.
     */
    public static bool IsEmpty(this Tilemap tilemap, Vector3Int cell)
    {
        return !tilemap.HasTile(cell);
    }

    /**
     * Returns the number of cells in this Tilemap.
     */
    public static int Size(this Tilemap tilemap)
    {
        return tilemap.size.x * tilemap.size.y;
    }

    /**
     * Returns the number of tiles in this Tilemap.
     */
    public static int Count(this Tilemap tilemap)
    {
        return tilemap.Count(tilemap.HasTile);
    }

    /**
     * Returns the number of cells that satisfy the predicate specified.
     */
    public static int Count(this Tilemap tilemap, Func<Vector3Int, bool> predicate)
    {
        return tilemap.Where(predicate).Count();
    }

    /**
     * Returns all cells in this Tilemap.
     */
    public static IEnumerable<Vector3Int> AllPositions(this Tilemap tilemap)
    {
        foreach (Vector3Int position in tilemap.cellBounds.allPositionsWithin)
        {
            yield return position;
        }
    }

    /**
     * Returns all cells with a tile in this Tilemap.
     */
    public static IEnumerable<Vector3Int> AllTiles(this Tilemap tilemap)
    {
        return tilemap.Where(tilemap.HasTile);
    }

    /**
     * Returns all cells that satisfy the predicate specified.
     */
    public static IEnumerable<Vector3Int> Where(this Tilemap tilemap, Func<Vector3Int, bool> predicate)
    {
        return tilemap.AllPositions().Where(predicate);
    }

    /**
     * Returns all cells with the tile specified.
     */ 
    public static IEnumerable<Vector3Int> Where(this Tilemap tilemap, TileBase tile)
    {
        return tilemap.Where(cell => tilemap.GetTile(cell) == tile);
    }

    /**
     * Returns the four cells orthogonal to the cell specified.
     */
    public static IEnumerable<Vector3Int> Get4Neighbors(this Tilemap tilemap, Vector3Int cell)
    {
        List<Vector3Int> neighbors = new List<Vector3Int>
        {
            cell + Vector3Int.left,
            cell + Vector3Int.right,
            cell + Vector3Int.up,
            cell + Vector3Int.down,
        };

        /* Remove neighbors out of bounds. */
        return neighbors.Where(tilemap.cellBounds.Contains);
    }

    /**
     * Returns the four cells diagonal to the cell specified.
     */
    public static IEnumerable<Vector3Int> GetDiagonals(this Tilemap tilemap, Vector3Int cell)
    {
        List<Vector3Int> neighbors = new List<Vector3Int>
        {
            cell + Vector3Int.left + Vector3Int.up,
            cell + Vector3Int.left + Vector3Int.down,
            cell + Vector3Int.right + Vector3Int.up,
            cell + Vector3Int.right + Vector3Int.down,
        };

        /* Remove neighbors out of bounds. */
        return neighbors.Where(tilemap.cellBounds.Contains);
    }

    /**
     * Returns the eight cells around the cell specified.
     */
    public static IEnumerable<Vector3Int> Get8Neighbors(this Tilemap tilemap, Vector3Int cell)
    {
        return tilemap.Get4Neighbors(cell).Concat(tilemap.GetDiagonals(cell));
    }

    /**
     * Performs the specified Action on each cell of this Tilemap.
     */
    public static void ForEach(this Tilemap tilemap, Action<Vector3Int> action)
    {
        tilemap.AllPositions().ForEach(action);
    }

    /**
     * Sets the tile specified at all the coordinates specified.
     */
    public static void SetTiles(this Tilemap tilemap, IEnumerable<Vector3Int> cells, TileBase tile)
    {
        cells.ForEach(cell => tilemap.SetTile(cell, tile));
    }

    /**
     * Sets the color of a cell in the Unity editor only.
     */
    public static void SetDebugColor(this Tilemap tilemap, Vector3Int cell, Color color)
    {
        #if UNITY_EDITOR
            tilemap.SetColor(cell, color);
        #endif
    }
}
