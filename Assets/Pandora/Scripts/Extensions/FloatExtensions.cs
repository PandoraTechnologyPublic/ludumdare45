﻿using UnityEngine;

/**
 * Extensions for float.
 */
public static class FloatExtensions
{
    /**
     * Returns whether this float is between the two float specified (both inclusive).
     */
    public static bool IsBetween(this float x, float a, float b)
    {
        return x >= Mathf.Min(a, b) && x <= Mathf.Max(a, b);
    }
}
