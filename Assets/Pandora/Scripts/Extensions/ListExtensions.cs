﻿using System.Collections.Generic;
using System.Linq;

/**
 * Extensions for List<T>.
 */
public static class ListExtensions
{
    /**
     * Adds the element specified to this List if it doesn't contain it already.
     */
    public static void AddOnce<T>(this List<T> list, T element)
    {
        if (!list.Contains(element))
        {
            list.Add(element);
        }
    }

    /**
     * Removes and returns the first element of this List.
     */
    public static T Dequeue<T>(this List<T> list)
    {
        T first = list.First();
        list.Remove(first);

        return first;
    }

    /**
     * Swaps the two elements at the indexes specified.
     */
    public static void Swap<T>(this List<T> list, int i, int j)
    {
        T element = list[i];
        list[i] = list[j];
        list[j] = element;
    }

    /**
     * Returns a random element without picking the same element twice in a row.
     */
    public static T GetRandomWithoutConsecutive<T>(this List<T> list)
    {
        if (list.IsEmpty())
        {
            return default(T);
        }

        if (list.Count == 1)
        {
            return list[0];
        }

        /* Ignore index 0 when picking element. */
        int pickedIndex = list.Skip(1).GetRandomIndex();

        /* Move picked element at index 0. */
        list.Swap(pickedIndex, 0);

        return list[0];
    }
}
