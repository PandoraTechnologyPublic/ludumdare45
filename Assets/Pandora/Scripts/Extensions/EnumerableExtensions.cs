﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

/**
 * Extensions for IEnumerable<T>.
 */
public static class EnumerableExtensions
{
    /**
     * Returns whether this IEnumerable does not contain any element.
     */
    public static bool IsEmpty<T>(this IEnumerable<T> source)
    {
        return !source.Any();
    }

    /**
     * Returns all elements of this IEnumerable except the one specified.
     */
    public static IEnumerable<T> Except<T>(this IEnumerable<T> source, T element)
    {
        return source.Except(new[] { element });
    }

    /**
     * Skips the last elements of this IEnumerable and returns the remaining elements.
     */
    public static IEnumerable<T> SkipLast<T>(this IEnumerable<T> source, int count)
    {
        return source.Reverse().Skip(count).Reverse();
    }

    /**
     * Performs the specified Action on each element of this IEnumerable.
     */
    public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
    {
        source.ToList().ForEach(action);
    }

    /**
     * Returns all pairs of elements from this IEnumerable.
     */
    public static IEnumerable<(T, T)> Pairs<T>(this IEnumerable<T> source)
    {
        List<T> list = source.ToList();

        for (int i = 0; i < list.Count; i++)
        {
            for (int j = i + 1; j < list.Count; j++)
            {
                yield return (list[i], list[j]);
            }
        }
    }

    /**
     * Returns all pairs (a, b) where a is in this IEnumerable and b is in the IEnumerable specified.
     */
    public static IEnumerable<(T, T)> Product<T>(this IEnumerable<T> first, IEnumerable<T> second)
    {
        return
        from a in first
        from b in second
        select (a, b);
    }

    /**
     * Returns the elements of this IEnumerable in a randomized order.
     */
    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
    {
        List<T> list = source.ToList();

        for (int i = 0; i < list.Count - 1; i++)
        {
            list.Swap(i, Random.Range(i, list.Count));
        }

        return list;
    }

    /**
     * Returns a Queue containing the elements of this IEnumerable.
     */
    public static Queue<T> ToQueue<T>(this IEnumerable<T> source)
    {
        return new Queue<T>(source);
    }

    /**
     * Returns a random index of this IEnumerable.
     */
    public static int GetRandomIndex<T>(this IEnumerable<T> source)
    {
        return Random.Range(0, source.Count());
    }

    /**
     * Returns a random element of this IEnumerable.
     */
    public static T GetRandom<T>(this IEnumerable<T> source)
    {
        return source.ElementAtOrDefault(GetRandomIndex(source));
    }

    /**
     * Returns a random element that satisfies the predicate specified.
     */
    public static T GetRandom<T>(this IEnumerable<T> source, Func<T, bool> predicate)
    {
        return source.Where(predicate).GetRandom();
    }
}
