using System;
using System.Collections;
using UnityEngine;

namespace Pandora.Scripts
{
    public class ResourcesProcessingComponent : MonoBehaviour
    {
        private Coroutine _processingCoroutine;

        public bool ProcessResources(RecipeData recipe, Action<Resource> callback)
        {
            if (_processingCoroutine == null)
            {
                _processingCoroutine = StartCoroutine(ProcessResourcesCoroutine(recipe, callback));
                return true;
            }
            else
            {
                return false;
            }
        }

        private IEnumerator ProcessResourcesCoroutine(RecipeData recipe, Action<Resource> callback)
        {
            yield return new WaitForSeconds(recipe._processingTime);

            callback(recipe._outputResource);

            _processingCoroutine = null;
        }

        public bool IsProcessingResources()
        {
            return _processingCoroutine != null;
        }
    }
}