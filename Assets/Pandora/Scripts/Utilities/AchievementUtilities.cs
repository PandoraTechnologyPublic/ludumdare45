using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pandora.Scripts.Utilities
{
    public static class AchievementUtilities
    {
        private static readonly string PLAYER_ACHIEVEMENT_ID = "PLAYER_ACHIEVEMENT_ID";
        
        public static void UpdateAchievement(ResourceType resourceType)
        {
            if (resourceType != ResourceType.NONE)
            {
                string playerAchievement = PlayerPrefs.GetString(PLAYER_ACHIEVEMENT_ID, "");
                char[] separators = {';'};
                string[] unlockedResources = playerAchievement.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                bool isNewUnlock = true;

                foreach (string unlockedResource in unlockedResources)
                {
                    if (((ResourceType) Enum.Parse(typeof(ResourceType), unlockedResource)) == resourceType)
                    {
                        isNewUnlock = false;
                        break;
                    }
                }

                if (isNewUnlock)
                {
                    playerAchievement += ";" + resourceType;
                    PlayerPrefs.SetString(PLAYER_ACHIEVEMENT_ID, playerAchievement);
                    PlayerPrefs.Save();
                }
            }
        }

        public static List<ResourceType> GetAchievements()
        {
            List<ResourceType> achievements = new List<ResourceType>();
            string playerAchievement = PlayerPrefs.GetString(PLAYER_ACHIEVEMENT_ID, "");
            char[] separators = {';'};
            string[] unlockedResources = playerAchievement.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            
            foreach (string unlockedResource in unlockedResources)
            {
                achievements.Add((ResourceType) Enum.Parse(typeof(ResourceType), unlockedResource));
            }

            return achievements;
        }
    }
}