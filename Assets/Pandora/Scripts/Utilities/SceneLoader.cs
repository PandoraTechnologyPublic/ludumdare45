﻿using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * Component for loading scenes. 
 */
public class SceneLoader : MonoBehaviour
{
    /**
    * Reloads the active scene.
    */
    public void ReloadScene()
    {
        LoadScene(SceneManager.GetActiveScene().name);
    }

    /**
     * Loads the scene with the name specified.
     */
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    /**
     * Loads the scene with the index specified.
     */
    public void LoadScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    /**
     * Loads the scene with the next index.
     */
    public void LoadNextScene()
    {
        LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
