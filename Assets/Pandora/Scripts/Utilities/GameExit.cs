﻿using UnityEngine;

/**
 * Component for exiting the game.
 */
public class GameExit : MonoBehaviour
{
    /**
     * Exits the game.
     */
    public void ExitGame()
    {
#if UNITY_EDITOR
        {
            UnityEditor.EditorApplication.isPlaying = false;
        }
#else
        {
            Application.Quit();
        }
#endif
    }
}