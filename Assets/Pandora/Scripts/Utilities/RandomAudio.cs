﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Component for playing AudioClips at random.
 */
public class RandomAudio : MonoBehaviour
{
    /**
     * AudioSource the clips are played on.
     */
    [SerializeField] private AudioSource audioSource;

    /**
     * Whether a random clip is played on awake.
     */
    [SerializeField] private bool playOnAwake;

    /**
     * Whether another clip is played after one is finished.
     */
    [SerializeField] private bool loop;

    /**
     * Minimum time between the end of a clip and the start of the next one.
     */
    [SerializeField] private float betweenDelayMin;

    /**
     * Maximum time between the end of a clip and the start of the next one.
     */
    [SerializeField] private float betweenDelayMax;

    /**
     * Set of AudioClips from which random clips are chosen.
     */
    [SerializeField] private List<AudioClip> clips;

    /**
     * Plays a random clip on Awake, if enabled.
     */
    private void Awake()
    {
        if (playOnAwake)
        {
            Play();
        }
    }

    /**
     * Plays a random clip.
     */
    public void Play()
    {
        StartCoroutine(PlayRandomSounds());
    }

    /**
     * Coroutine to play random clips.
     */
    private IEnumerator PlayRandomSounds()
    {
        do
        {
            AudioClip clip = clips.GetRandomWithoutConsecutive();

            if (IsVisible())
            {
                audioSource.PlayOneShot(clip);
            }

            yield return new WaitForSeconds(clip.length + Random.Range(betweenDelayMin, betweenDelayMax));
        }
        while (loop);
    }

    /**
     * Returns whether this GameObject appears on screen.
     */
    public bool IsVisible()
    {
        Vector3 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

        return positionOnScreen.x.IsBetween(0, 1) && positionOnScreen.y.IsBetween(0, 1) && positionOnScreen.z >= 0;
    }

    public void PlayOneShot()
    {
        AudioClip clip = clips.GetRandomWithoutConsecutive();

        if (IsVisible())
        {
            if (transform.root.name == "Player" && gameObject.name == "Sprites" && transform.position.x > 500)
            {
                PlayWoodFootstepsInstead();
                return;
            }

            audioSource.PlayOneShot(clip);
        }
    }

    // this is ugly as f*ck
    private void PlayWoodFootstepsInstead()
    {
        woodSteps.PlayOneShot();
    }

    [SerializeField] private RandomAudio woodSteps;
}