﻿using UnityEngine;
using UnityEngine.Events;

/**
 * Invokes callbacks upon inputs.
 */
public class InputTrigger : MonoBehaviour
{
    /**
     * Input that triggers callbacks.
     */
    [SerializeField] private KeyCode input;

    /**
     * Invoked when the input is pressed down.
     */
    [SerializeField] private UnityEvent onButtonDown;

    /**
     * Invoked every frame while the input is held down.
     */
    [SerializeField] private UnityEvent onButtonHeld;

    /**
     * Invoked when the input is released.
     */
    [SerializeField] private UnityEvent onButtonUp;

    /**
     * Invokes the corresponding event when the input is pressed, held down, or released.
     */
    private void Update()
    {
        if (Input.GetKeyDown(input))
        {
            onButtonDown.Invoke();
        }

        if (Input.GetKey(input))
        {
            onButtonHeld.Invoke();
        }

        if (Input.GetKeyUp(input))
        {
            onButtonUp.Invoke();
        }
    }
}
