using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.Scripts
{
    [CreateAssetMenu(fileName = "RecipeData", menuName = "RecipeData", order = 0)]
    public class RecipeData : ScriptableObject
    {
        public ResourceType _processingElement;
        public int _processingTime;
        public List<Resource> _inputResources;
        public Resource _outputResource;
    }
}