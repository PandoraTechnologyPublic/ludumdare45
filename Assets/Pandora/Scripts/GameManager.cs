﻿using System;
using System.Collections.Generic;
using Pandora;
using Pandora.Scripts;
using UnityEngine;
using UnityEngine.Assertions;

public class GameManager : MonoBehaviour
{
    [SerializeField] private MainUiLayout _mainUiLayout;
    
    private static readonly Dictionary<Type, Service> _services = new Dictionary<Type, Service>();

    private void Awake()
    {
        MainUiService mainUiService = new MainUiService();
        GameStateService gameStateService = new GameStateService();
        PlayerDataService playerDataService = new PlayerDataService();
        ResourceDataService resourceDataService = new ResourceDataService();

        _services.Clear();
        
        mainUiService.Initialise(_mainUiLayout, gameStateService, playerDataService, resourceDataService);
        _services.Add(typeof(MainUiService), mainUiService);
        
        gameStateService.Initialise(mainUiService, playerDataService, resourceDataService);
        _services.Add(typeof(GameStateService), gameStateService);
        
        playerDataService.Initialise();
        _services.Add(typeof(PlayerDataService), playerDataService);
        
        resourceDataService.Initialise();
        _services.Add(typeof(ResourceDataService), resourceDataService);
    }

    private void Update()
    {
        (_services[typeof(GameStateService)] as GameStateService).Update(Time.deltaTime);
    }

//    public static TYPE_SERVICE GetService<TYPE_SERVICE>() where TYPE_SERVICE : Service
//    {
//        Type type = typeof(TYPE_SERVICE);
//        TYPE_SERVICE service = _services[type] as TYPE_SERVICE;
//        Assert.IsTrue(service.IsInitialised());
//        return service;
//    }
}
