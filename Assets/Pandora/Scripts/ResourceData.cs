using Pandora.Scripts;
using UnityEngine;

namespace Pandora
{
    [CreateAssetMenu(fileName = "ResourceData", menuName = "ResourceData", order = 0)]
    public class ResourceData : ScriptableObject
    {
        public ResourceType _resourceType;
        public int _cost;
        public float _carrySpriteVerticalOffset;
        public Sprite _sprite;
        public InteractingEntityComponent _prefab;
        public PandoraTile _tile;
        public bool _isGrowable;
        public AudioClip pickUpSound;
    }
}