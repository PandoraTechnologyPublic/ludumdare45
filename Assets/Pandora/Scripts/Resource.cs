using System;
using System.Collections;
using Pandora.Scripts;

namespace Pandora
{
    [Serializable]
    public class Resource
    {
        public ResourceType _resourceType;
        public int _quantity;

        public Resource(ResourceType resourceType, int quantity = 1)
        {
            _resourceType = resourceType;
            _quantity = quantity;
        }
    }
}