using UnityEngine;

namespace Pandora.Scripts
{
    public class PandoraTile : RuleTile
    {
        public bool isInteractable;
        public ResourceType resourceType;
        public PandoraTile fallbackTile;
        public bool isPlowable;
        public bool isCollectable;
    }
}