using System;
using Pandora.Scripts;
using UnityEngine;

namespace Pandora
{
    public class ResourceProducer : MonoBehaviour
    {
        [SerializeField] private ResourceType _resourceType;
        [SerializeField] private float _resourceProductionTime = 10.0f;
        [SerializeField] private Animator _resourceProducterAnimator;

        private float _elapsedProductionTime;

        private void Awake()
        {
            _elapsedProductionTime = 0;
        }

        public ResourceType CollectResource()
        {
            ResourceType producedResource = ResourceType.NONE;
            
            if (_elapsedProductionTime >= _resourceProductionTime)
            {
                producedResource = _resourceType;
                _elapsedProductionTime = 0;
                //_resourceProducterAnimator.SetBool("moving", false);
            }

            return producedResource;
        }

        public bool IsCollectable()
        {
            return _elapsedProductionTime >= _resourceProductionTime;
        }

        public ResourceType GetResourceType()
        {
            return _resourceType;
        }

        public float GetResourceProductionTime()
        {
            return _resourceProductionTime;
        }

        private void Update()
        {
            _elapsedProductionTime += Time.deltaTime;
            
            if (_elapsedProductionTime >= _resourceProductionTime)
            {
                //_resourceProducterAnimator.SetBool("moving", true);
            }
        }
    }
}