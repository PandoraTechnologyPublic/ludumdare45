using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Pandora.Scripts
{
    public class InteractionCollider : MonoBehaviour
    {
        public readonly List<GameObject> collidingItems = new List<GameObject>();

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.GetType() != typeof(Tilemap))
            {
                collidingItems.Add(other.gameObject);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (collidingItems.Contains(other.gameObject))
            {
                collidingItems.Remove(other.gameObject);
            }
        }
    }
}