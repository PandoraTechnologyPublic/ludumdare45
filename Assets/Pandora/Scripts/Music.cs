﻿using UnityEngine;

public class Music : MonoBehaviour
{
    [SerializeField] public AudioSource audioSource;

    [SerializeField] private int repeatEvery;

    void Start()
    {
        InvokeRepeating("PlayMusic", 0, repeatEvery);
    }

    private void PlayMusic()
    {
        if (!audioSource.isPlaying)
            audioSource.Play();
    }
}
