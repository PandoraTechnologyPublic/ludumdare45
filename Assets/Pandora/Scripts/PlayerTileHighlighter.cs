using System;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Pandora.Scripts
{
    [RequireComponent(typeof(InteractionController))]
    public class PlayerTileHighlighter : MonoBehaviour
    {
        public static readonly Vector3Int NO_POSITION = new Vector3Int(Int32.MaxValue, Int32.MaxValue, Int32.MaxValue);

        [SerializeField] private Tilemap _tilemap;
        [SerializeField] private Tilemap _highlightTilemap;
        [SerializeField] private Tile _highlightTile;

        private InteractionController _interactionController;

        private Vector3Int _currentHighlightedTilePosition;
        private ResourceDataService _resourceDataService;

        private void Awake()
        {
            _interactionController = GetComponent<InteractionController>();
        }

        public void Initialise(ResourceDataService resourceDataService)
        {
            _resourceDataService = resourceDataService;
        }

        private void Update()
        {
            Vector3Int playerInteractingCell = _interactionController.GetCollidingCellPosition(_highlightTilemap);

            //if (_currentHighlightedTilePosition != cellPosition)
            {
                _highlightTilemap.SetTile(_currentHighlightedTilePosition, null);
                
                PandoraTile playerInteractingTile = _tilemap.GetTile<PandoraTile>(playerInteractingCell);

                if (playerInteractingTile != null)
                {
                    bool doesHighlightTile = false;
                    PlayerInventoryComponent playerInventoryComponent = GetComponent<PlayerInventoryComponent>();
                    ResourceType currentCarriedResource = playerInventoryComponent.GetCarriedResource();
                    
                    if (currentCarriedResource == ResourceType.NONE)
                    {
                        doesHighlightTile = playerInteractingTile.isInteractable;
                    }
                    else
                    {
                        ResourceData resourceData = _resourceDataService.GetResourceData(currentCarriedResource);
                        
                        if (currentCarriedResource == ResourceType.PLOWED_LAND)
                        {
                            doesHighlightTile = playerInteractingTile.isPlowable;
                        }
                        else if (resourceData._isGrowable)
                        {
                            doesHighlightTile = playerInteractingTile.resourceType == ResourceType.PLOWED_LAND;
                        }
                    }
                    
                    if (doesHighlightTile)
                    {
                        Tile tile = ScriptableObject.Instantiate(_highlightTile);
                        _highlightTilemap.SetTile(playerInteractingCell, tile);
                        _currentHighlightedTilePosition = playerInteractingCell;
                    }
                    else
                    {
                        _currentHighlightedTilePosition = NO_POSITION;
                    }
                }
                else
                {
                    _currentHighlightedTilePosition = NO_POSITION;
                }
            }
        }

        public PandoraTile GetHighlightedTile()
        {
            PandoraTile tile = null;

            if (_currentHighlightedTilePosition != NO_POSITION)
            {
                tile = _tilemap.GetTile<PandoraTile>(_currentHighlightedTilePosition);
            }

            return tile;
        }

        public Vector3Int GetHighlitedTilePosition()
        {
            return _currentHighlightedTilePosition;
        }

        public void UnhighlightTile()
        {
            if (_currentHighlightedTilePosition != NO_POSITION)
            {
                _highlightTilemap.SetTile(_currentHighlightedTilePosition, null);
            }
        }
    }
}