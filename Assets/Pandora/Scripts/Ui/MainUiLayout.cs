using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Pandora.Scripts
{
    public class MainUiLayout : MonoBehaviour
    {
        [SerializeField] public IngameScreenUiLayout _ingameScreenUiLayout;
        [SerializeField] public MainMenuScreenUiLayout _mainMenuScreenUiLayout;
        [SerializeField] public PauseScreenUiLayout _pauseScreenUiLayout;

        public void Initialise(GameStateService gameStateService, PlayerDataService playerDataService, ResourceDataService resourceDataService)
        {
            _mainMenuScreenUiLayout.Initialise(gameStateService);
            _ingameScreenUiLayout.Initialise(playerDataService, resourceDataService);
            _pauseScreenUiLayout.Initialise(resourceDataService);
        }
    }
}