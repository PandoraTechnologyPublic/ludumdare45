using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Scripts
{
    public class AchievementLayout : MonoBehaviour
    {
        public Image _resourceImage;
        public RectTransform _achievedPanel;
    }
}