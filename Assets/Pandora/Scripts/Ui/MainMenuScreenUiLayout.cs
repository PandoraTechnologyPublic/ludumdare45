using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Pandora.Scripts
{
    public class MainMenuScreenUiLayout : MonoBehaviour
    {
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _exitButton;

        private GameStateService _gameStateService;

        public void Initialise(GameStateService gameStateService)
        {
            _gameStateService = gameStateService;
        }
        
        private void Awake()
        {
            _playButton.onClick.AddListener(OnPlayButton);
            _exitButton.onClick.AddListener(OnExitButton);
        }


        private void OnPlayButton()
        {
            _gameStateService.ChangeState<EnterGameState>();
        }

        private void OnExitButton()
        {
#if UNITY_EDITOR
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }
#else
        {
            Application.Quit();
        }
#endif
        }
    }
}