using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Scripts
{
    public class IngameScreenUiLayout : MonoBehaviour
    {
        [SerializeField] private TMP_Text _moneyText;
        [SerializeField] private RectTransform _displayBox;
        [SerializeField] private HorizontalLayoutGroup _itemPanel;
        [SerializeField] private Image _itemImagePrototype;
        
        private PlayerDataService _playerDataService;
        private ResourceDataService _resourceDataService;
        
        private readonly List<Image> _itemImages = new List<Image>();

        public void Initialise(PlayerDataService playerDataService, ResourceDataService resourceDataService)
        {
            _itemImagePrototype.gameObject.SetActive(false);
            _playerDataService = playerDataService;
            _resourceDataService = resourceDataService;
        }

        private void Update()
        {
            _moneyText.SetText(_playerDataService.GetMoney().ToString());
            
            List<ResourceType> resourceTypes = _playerDataService.GetInputResourceToDisplay();
            Queue<Image> availableImages = new Queue<Image>();
            
            foreach (Image image in _itemImages)
            {
                image.gameObject.SetActive(false);
                availableImages.Enqueue(image);
            }
            
            _itemImages.Clear();
            
            _displayBox.gameObject.SetActive(resourceTypes.Count > 0);
            
            if (resourceTypes.Count > 0)
            {
                foreach (ResourceType resourceType in resourceTypes)
                {
                    Image image = null;
                
                    if (availableImages.Count > 0)
                    {
                        image = availableImages.Dequeue();
                    }
                    else
                    {
                        image = GameObject.Instantiate(_itemImagePrototype, _itemPanel.transform);
                    }
                
                    _itemImages.Add(image);
                    image.sprite = _resourceDataService.GetResourceData(resourceType)._sprite;
                    image.gameObject.SetActive(true);
                }
            }
        }
    }
}