using System;
using System.Collections.Generic;
using Pandora.Scripts.Utilities;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Pandora.Scripts
{
    public class PauseScreenUiLayout : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource;

        [SerializeField] private Music music;
        [SerializeField] private RectTransform _goalPanel;
        [SerializeField] private AchievementLayout _achievementPrototype;
        
        private ResourceDataService _resourceDataService;
        
        private readonly List<AchievementLayout> _achievementLayouts = new List<AchievementLayout>();

        private void OnEnable()
        {
            music.audioSource.Pause();
            audioSource.Play();
            
            _achievementPrototype.gameObject.SetActive(false);

            foreach (AchievementLayout achievementLayout in _achievementLayouts)
            {
                GameObject.Destroy(achievementLayout.gameObject);
            }
            
            HashSet<ResourceType> nonShownResources = new HashSet<ResourceType> {ResourceType.NONE, ResourceType.MILL, ResourceType.OVEN, ResourceType.CRAFTING_TABLE, ResourceType.FENCE, ResourceType.PLOWED_LAND};

            List<ResourceType> achievements = AchievementUtilities.GetAchievements();
            foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
            {
                if (!nonShownResources.Contains(resourceType))
                {
                    ResourceData resourceData = _resourceDataService.GetResourceData(resourceType);

                    AchievementLayout instantiatedIngredient = GameObject.Instantiate(_achievementPrototype, _goalPanel, false);

                    instantiatedIngredient._resourceImage.sprite = resourceData._sprite;
                    instantiatedIngredient._achievedPanel.gameObject.SetActive(achievements.Contains(resourceType));
                    instantiatedIngredient.gameObject.SetActive(true);
                    _achievementLayouts.Add(instantiatedIngredient);
                }
            }
        }

        public void Initialise(ResourceDataService resourceDataService)
        {
            _resourceDataService = resourceDataService;
        }

        private void OnDisable()
        {
            if( music.audioSource != null)
            {
                music.audioSource.Play();
            }
        }
        public void BackToMenu()
        {
            SceneManager.LoadScene(0);
        }
    }
}