using System;
using System.Collections.Generic;
using Pandora.Scripts;
using Pandora.Scripts.Utilities;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora
{
    public class TransformBox : MonoBehaviour
    {
        
        [SerializeField] private SpriteRenderer _processedResourceSprite;
        [SerializeField] private AudioClip finishedSound;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private ResourceType _processingElementType;

        private List<RecipeData> _handledRecipes;
        private Animator _animator;

        private TransformBoxState _state;
        private readonly Dictionary<ResourceType, int> _inputResources = new Dictionary<ResourceType, int>();
        private Resource _outputResource;

        private ResourceDataService _resourceDataService;
        private ResourcesProcessingComponent _resourcesProcessingComponent;

        private readonly Dictionary<List<Resource>, RecipeData> _recipesByInputResources = new Dictionary<List<Resource>, RecipeData>();

        public enum TransformBoxState
        {
            IDLE,
            PROCESSING,
            ENDED_PROCESSING
        }

        private void Awake()
        {
            _resourcesProcessingComponent = GetComponent<ResourcesProcessingComponent>();
            _animator = GetComponentInChildren<Animator>();
            _processedResourceSprite.sprite = null;
            _state = TransformBoxState.IDLE;

            Assert.IsTrue(ResourceDataService._processingElements.Contains(_processingElementType), $"{nameof(_processingElementType)} is not defined with a valid value {string.Join(" | ", ResourceDataService._processingElements)}");
        }

        public void Initialise(ResourceDataService resourceDataService)
        {
            _resourceDataService = resourceDataService;
            _handledRecipes = resourceDataService.GetRecipes(_processingElementType);
            
            foreach (RecipeData handledRecipe in _handledRecipes)
            {
                handledRecipe._inputResources.Sort((resource, resource1) => resource._resourceType - resource1._resourceType);
                _recipesByInputResources.Add(handledRecipe._inputResources, handledRecipe);
            }
        }

        public void ShowInputResources(PlayerDataService playerDataService)
        {
            playerDataService.SetDisplayResourceList(_inputResources);
        }

        public bool OnInteract(Resource deliveredResource)
        {
            bool hasInteractionSucceeded = false;

            if (IsResourceHandled(deliveredResource) && IsResourceHandled(deliveredResource, _inputResources) && _state == TransformBoxState.IDLE)
            {
                if (_inputResources.ContainsKey(deliveredResource._resourceType))
                {
                    _inputResources[deliveredResource._resourceType] += deliveredResource._quantity;
                }
                else
                {
                    _inputResources.Add(deliveredResource._resourceType, deliveredResource._quantity);
                }

                List<RecipeData> matchingRecipes = GetMatchingRecipes();

                if (matchingRecipes.Count > 0)
                {
                    foreach (RecipeData matchingRecipe in matchingRecipes)
                    {
                        if (IsRecipeComplete(matchingRecipe))
                        {
                            _resourcesProcessingComponent.ProcessResources(matchingRecipe, OnResourcesProcessed);
//                            hasInteractionSucceeded = true;
                            _state = TransformBoxState.PROCESSING;
                            if (_animator != null)
                            {
                                _animator.SetBool("baking", true);
                            }

                            break;
                        }
                    }
                }
                
                hasInteractionSucceeded = true;
            }

            return hasInteractionSucceeded;
        }

        public Resource RecoverItem()
        {
            Resource recoveredResource = null;

            if (_state == TransformBoxState.IDLE)
            {
                foreach (KeyValuePair<ResourceType,int> keyValuePair in _inputResources)
                {
                    recoveredResource = new Resource(keyValuePair.Key);
                    
                    if (keyValuePair.Value <= 1)
                    {
                        _inputResources.Remove(keyValuePair.Key);
                    }
                    else
                    {
                        _inputResources[keyValuePair.Key] -= 1;
                    }
                    break;
                }
            }

            return recoveredResource;
        }

        public Resource OnCollect()
        {
            Resource collectedResource = null;

            if (_state == TransformBoxState.ENDED_PROCESSING)
            {
                collectedResource = _outputResource;

                _state = TransformBoxState.IDLE;
                _outputResource = null;
                _processedResourceSprite.sprite = null;
                _processedResourceSprite.gameObject.SetActive(false);

            }

            return collectedResource;
        }

        private void OnResourcesProcessed(Resource outputResource)
        {
            AchievementUtilities.UpdateAchievement(outputResource._resourceType);
            
            _state = TransformBoxState.ENDED_PROCESSING;
            _outputResource = outputResource;
            _inputResources.Clear();
            _processedResourceSprite.sprite = _resourceDataService.GetResourceData(outputResource._resourceType)._sprite;
            _processedResourceSprite.gameObject.SetActive(true);
            if (_animator != null)
            {
                _animator.SetBool("baking", false);
                if (finishedSound != null)
                    audioSource.PlayOneShot(finishedSound);
                else
                {
                    audioSource.PlayOneShot(_resourceDataService.GetResourceData(outputResource._resourceType).pickUpSound);
                }
            }
        }

        public TransformBoxState GetState()
        {
            return _state;
        }

        private List<RecipeData> GetMatchingRecipes()
        {
            List<RecipeData> filteredRecipes = new List<RecipeData>(_handledRecipes);

            foreach (RecipeData recipe in _handledRecipes)
            {
                foreach (ResourceType resourceType in _inputResources.Keys)
                {
                    int quantity = _inputResources[resourceType];

                    if (!DoesRecipeContainResource(recipe, resourceType, quantity))
                    {
                        filteredRecipes.Remove(recipe);
                    }
                }
            }

            return filteredRecipes;
        }

        private bool IsResourceHandled(Resource resource)
        {
            foreach (RecipeData recipe in GetMatchingRecipes())
            {
                if (DoesRecipeContainResource(recipe, resource._resourceType, resource._quantity))
                {
                    return true;
                }
            }

            return false;
        }        
        
        private bool IsResourceHandled(Resource resource, Dictionary<ResourceType, int> currentResource)
        {
            int resourceCount = 1;
            
            if (currentResource.TryGetValue(resource._resourceType, out int currentResourceCount))
            {
                resourceCount += currentResourceCount;
            }
            
            foreach (RecipeData recipe in GetMatchingRecipes())
            {
                if (DoesRecipeContainResource(recipe, resource._resourceType, resourceCount))
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsRecipeComplete(RecipeData recipe)
        {
            int recipeResourcesCount = recipe._inputResources.Count;
            int currentResourcesCount = 0;

            foreach (ResourceType resourceType in _inputResources.Keys)
            {
                int quantity = _inputResources[resourceType];
                if (DoesRecipeContainResource(recipe, resourceType, quantity))
                {
                    currentResourcesCount++;
                }
            }

            return currentResourcesCount == recipeResourcesCount;
        }

        public bool DoesRecipeContainResource(RecipeData recipe, ResourceType resourceType, int quantity)
        {
            foreach (Resource resource in recipe._inputResources)
            {
                if (resource._resourceType == resourceType && quantity <= resource._quantity)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsEmpty()
        {
            return _inputResources.Count == 0;
        }
    }
}