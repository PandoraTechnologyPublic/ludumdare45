using System;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace Pandora.Scripts
{
    public class PlayerDataService : Service
    {
        private PlayerData _playerData;
        
        private List<ResourceType> _resourceTypesToDisplay = new List<ResourceType>();
    
        public new void Initialise()
        {
            base.Initialise();
            _playerData = new PlayerData();
        }

        public void AddMoney(int earnedMoney)
        {
            _playerData._money += earnedMoney;
        }

        public int GetMoney()
        {
            return _playerData._money;
        }

        public void PayMoney(int cost)
        {
            _playerData._money -= cost;
            Assert.IsTrue(_playerData._money >= 0);
        }

        public void SetDisplayResourceList(Dictionary<ResourceType,int> inputResources)
        {
            _resourceTypesToDisplay.Clear();
            
            if (inputResources != null)
            {
                foreach (KeyValuePair<ResourceType,int> inputResource in inputResources)
                {
                    for (int resourceIndex = 0; resourceIndex < inputResource.Value; resourceIndex++)
                    {
                        _resourceTypesToDisplay.Add(inputResource.Key);
                    }
                }
            }
        }

        public List<ResourceType> GetInputResourceToDisplay()
        {
            return _resourceTypesToDisplay;
        }
    }
}