namespace Pandora.Scripts
{
    public abstract class Service
    {
        private bool _isInitialised;

        protected void Initialise()
        {
            _isInitialised = true;
        }

        public bool IsInitialised()
        {
            return _isInitialised;
        }
    }
}