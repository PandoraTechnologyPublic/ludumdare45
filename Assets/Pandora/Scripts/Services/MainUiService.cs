namespace Pandora.Scripts
{
    public class MainUiService : Service
    {
        private MainUiLayout _mainUiLayout;

        public void Initialise(MainUiLayout mainUiLayout, GameStateService gameStateService, PlayerDataService playerDataService, ResourceDataService resourceDataService)
        {
            base.Initialise();

            _mainUiLayout = mainUiLayout;

            mainUiLayout.Initialise(gameStateService, playerDataService, resourceDataService);
            
            _mainUiLayout._ingameScreenUiLayout.gameObject.SetActive(false);
            _mainUiLayout._mainMenuScreenUiLayout.gameObject.SetActive(false);
            _mainUiLayout._pauseScreenUiLayout.gameObject.SetActive(false);
        }
        

        public void ShowPauseScreen()
        {
            _mainUiLayout._pauseScreenUiLayout.gameObject.SetActive(true);
        }
        
        public void HidePauseScreen()
        {
            _mainUiLayout._pauseScreenUiLayout.gameObject.SetActive(false);
        }

        public void ShowIngameScreen()
        {
            _mainUiLayout._ingameScreenUiLayout.gameObject.SetActive(true);
        }

        public void HideIngameScreen()
        {
            _mainUiLayout._ingameScreenUiLayout.gameObject.SetActive(false);
        }

        public void ShowMainMenuScreen()
        {
            _mainUiLayout._mainMenuScreenUiLayout.gameObject.SetActive(true);
        }

        public void HideMainMenuScreen()
        {
            _mainUiLayout._mainMenuScreenUiLayout.gameObject.SetActive(false);
        }
    }
}