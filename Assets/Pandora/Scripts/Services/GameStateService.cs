using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pandora.Scripts
{
    public class GameStateService : Service
    {
        private readonly Dictionary<Type, State> _states = new Dictionary<Type, State>();
        private State _currentState;
        private MainUiService _mainUiService;
        
        public void Initialise(MainUiService mainUiService, PlayerDataService playerDataService, ResourceDataService resourceDataService)
        {
            base.Initialise();

            _mainUiService = mainUiService;
            
            _states.Add(typeof(EnterGameState), new EnterGameState(_mainUiService, this, playerDataService, resourceDataService));
            _states.Add(typeof(MainMenuScreenState), new MainMenuScreenState(_mainUiService));
            _states.Add(typeof(IngameState), new IngameState(_mainUiService, this));
            _states.Add(typeof(PauseScreenState), new PauseScreenState(_mainUiService, this));
            _states.Add(typeof(ExitGameState), new ExitGameState(this));
            
            ChangeState<MainMenuScreenState>();
        }

        public void ChangeState<STATE_TYPE>()
        {
            if (_currentState != null)
            {
                _currentState.OnExit();
            }

            _currentState = _states[typeof(STATE_TYPE)];
            _currentState.OnEnter();
        }

        public bool IsCurrentState<TYPE_STATE>()
        {
            return _currentState.GetType() == typeof(TYPE_STATE);
        }


        public void Update(float deltaTime)
        {
            _currentState.OnUpdate(deltaTime);
        }
    }
}