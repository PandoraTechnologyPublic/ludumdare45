using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.Scripts
{
    public class ResourceDataService : Service
    {
        public static readonly HashSet<ResourceType> _processingElements = new HashSet<ResourceType> {ResourceType.MILL, ResourceType.OVEN, ResourceType.CRAFTING_TABLE};
        
        private readonly Dictionary<ResourceType, ResourceData> _resourcesData = new Dictionary<ResourceType, ResourceData>();
        private readonly List<RecipeData> _recipesData = new List<RecipeData>();
        
        public new void Initialise()
        {
            ResourceData[] resourcesData = Resources.LoadAll<ResourceData>("");
            
            foreach (ResourceData resourceData in resourcesData)
            {
                _resourcesData.Add(resourceData._resourceType, resourceData);
            }
        
            foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
            {
                if (resourceType != ResourceType.NONE)
                {
                    Assert.IsTrue(_resourcesData.ContainsKey(resourceType), 
                        $"{resourceType} not define it data in Resource folder");
                    Assert.IsTrue(_resourcesData[resourceType]._prefab != null || _resourcesData[resourceType]._tile != null, 
                        $"Missing resources in {resourceType}");// && !(_resourcesData[resourceType]._prefab != null && _resourcesData[resourceType]._tile != null));
                }
            }
            
            
            RecipeData[] recipesData = Resources.LoadAll<RecipeData>("Recipes/");
            
            foreach (RecipeData recipeData in recipesData)
            {
                foreach (Resource recipeDataInputResource in recipeData._inputResources)
                {
                    Assert.IsFalse(_processingElements.Contains(recipeDataInputResource._resourceType), 
                        $"The {nameof(recipeData._inputResources)} for the recipe {recipeData._outputResource._resourceType} is not correct : should not be {string.Join(" | ", ResourceDataService._processingElements)}");
                }
                Assert.IsFalse(_processingElements.Contains(recipeData._outputResource._resourceType), 
                    $"The {nameof(recipeData._outputResource)} for the recipe {recipeData._outputResource._resourceType} is not correct : should not be {string.Join(" | ", ResourceDataService._processingElements)}");
                Assert.IsTrue(_processingElements.Contains(recipeData._processingElement), 
                    $"The {nameof(recipeData._processingElement)} for the recipe {recipeData._outputResource._resourceType} is not defined : should be {string.Join(" | ", ResourceDataService._processingElements)}");
            }
            
            _recipesData.AddRange(recipesData);
        }

        public ResourceData GetResourceData(ResourceType resourceType)
        {
            return _resourcesData[resourceType];
        }

        public List<RecipeData> GetRecipes(ResourceType processingElementResourceType)
        {
            List<RecipeData> resultRecipes = new List<RecipeData>();
            
            foreach (RecipeData recipeData in _recipesData)
            {
                if (recipeData._processingElement == processingElementResourceType)
                {
                    resultRecipes.Add(recipeData);
                }
            }
            
            Assert.AreNotEqual(0, resultRecipes.Count, $"No recipe found for {processingElementResourceType}");

            return resultRecipes;
        }
    }
}