using UnityEngine;

namespace Pandora.Scripts
{
    public class ExitGameState : State
    {
        private readonly GameStateService _gameStateService;

        public ExitGameState(GameStateService gameStateService)
        {
            _gameStateService = gameStateService;
        }
        
        public void OnEnter()
        {
            Debug.Log($"Enter state {this.GetType()}");
        }

        public void OnExit()
        {
            Debug.Log($"Exit state {this.GetType()}");
        }

        public void OnUpdate(float timeStep)
        {            
            _gameStateService.ChangeState<MainMenuScreenState>();
        }
    }
}