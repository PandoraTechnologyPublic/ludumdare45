using UnityEngine;

namespace Pandora.Scripts
{
    public class MainMenuScreenState : State
    {
        private readonly MainUiService _mainUiService;

        public MainMenuScreenState(MainUiService mainUiService)
        {
            _mainUiService = mainUiService;
        }
        
        public void OnEnter()
        {
            _mainUiService.ShowMainMenuScreen();
            
            Debug.Log($"Enter state {this.GetType()}");
        }

        public void OnExit()
        {
            _mainUiService.HideMainMenuScreen();
            Debug.Log($"Exit state {this.GetType()}");
        }

        public void OnUpdate(float timeStep)
        {
        }
    }
}