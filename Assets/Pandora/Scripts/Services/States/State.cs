namespace Pandora.Scripts
{
    public interface State
    {
        void OnEnter();
        void OnExit();
        void OnUpdate(float timeStep);
    }
}