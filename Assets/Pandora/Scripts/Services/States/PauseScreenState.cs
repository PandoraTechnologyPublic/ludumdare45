using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora.Scripts
{
    public class PauseScreenState : State
    {
        private readonly MainUiService _mainUiService;
        private readonly GameStateService _gameStateService;

        public PauseScreenState(MainUiService mainUiService, GameStateService gameStateService)
        {
            _mainUiService = mainUiService;
            _gameStateService = gameStateService;
        }
        
        public void OnEnter()
        {
            Time.timeScale = 0.0f;
            _mainUiService.ShowPauseScreen();
            Debug.Log($"Enter state {this.GetType()}");
        }

        public void OnExit()
        {
            Time.timeScale = 1.0f;
            _mainUiService.HidePauseScreen();
            Debug.Log($"Exit state {this.GetType()}");
        }

        public void OnUpdate(float timeStep)
        {            
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _gameStateService.ChangeState<IngameState>();
            }
            else if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene(0);
                //_gameStateService.ChangeState<ExitGameState>();
            }
        }

        public void BackToGame()
        {
            _gameStateService.ChangeState<IngameState>();
        }
    }
}