using UnityEngine;

namespace Pandora.Scripts
{
    public class EnterGameState : State
    {
        private readonly MainUiService _mainUiService;
        private readonly GameStateService _gameStateService;
        private readonly PlayerDataService _playerDataService;
        private readonly ResourceDataService _resourceDataService;

        public EnterGameState(MainUiService mainUiService, GameStateService gameStateService, PlayerDataService playerDataService, ResourceDataService resourceDataService)
        {
            _mainUiService = mainUiService;
            _gameStateService = gameStateService;
            _playerDataService = playerDataService;
            _resourceDataService = resourceDataService;
        }

        public void OnEnter()
        {
            PlayerController playerController = GameObject.FindObjectOfType<PlayerController>();
            playerController.Initialise(_gameStateService);
            playerController.GetComponent<PlayerInventoryComponent>().Initialise(_resourceDataService);
            playerController.GetComponent<InteractionController>().Initialise(_resourceDataService, _playerDataService);
            playerController.GetComponent<PlayerTileHighlighter>().Initialise(_resourceDataService);

            DeliveryBox deliveryBox = GameObject.FindObjectOfType<DeliveryBox>();
            deliveryBox.Initialise(_playerDataService, _resourceDataService);

            TransformBox[] transformBoxes = GameObject.FindObjectsOfType<TransformBox>();

            foreach (TransformBox transformBox in transformBoxes)
            {
                transformBox.Initialise(_resourceDataService);
            }

            foreach (ShopItem shopItem in GameObject.FindObjectsOfType<ShopItem>())
            {
                shopItem.Initialise(_resourceDataService);
            }

            Debug.Log($"Enter state {this.GetType()}");
            
            Time.timeScale = 1.0f;
        }

        public void OnExit()
        {
            Debug.Log($"Exit state {this.GetType()}");
        }

        public void OnUpdate(float timeStep)
        {
            _gameStateService.ChangeState<IngameState>();
        }
    }
}