using UnityEngine;

namespace Pandora.Scripts
{
    public class IngameState : State
    {
        private readonly MainUiService _mainUiService;
        private readonly GameStateService _gameStateService;

        public IngameState(MainUiService mainUiService, GameStateService gameStateService)
        {
            _mainUiService = mainUiService;
            _gameStateService = gameStateService;
        }
        
        public void OnEnter()
        {
            Debug.Log($"Enter state {this.GetType()}");
            _mainUiService.ShowIngameScreen();
        }

        public void OnExit()
        {
            Debug.Log($"Exit state {this.GetType()}");
            _mainUiService.HideIngameScreen();
        }

        public void OnUpdate(float timeStep)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _gameStateService.ChangeState<PauseScreenState>();
            }
        }
    }
}