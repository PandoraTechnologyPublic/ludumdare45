﻿using System.Collections;
using UnityEngine;

public class Wander : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;

    [SerializeField] private new Rigidbody2D rigidbody2D;

    [SerializeField] private Animator _resourceProducterAnimator;

    [SerializeField] private int walkDistance;

    [SerializeField] private float walkSpeed;

    [SerializeField] private int betweenDelayMin;

    [SerializeField] private int betweenDelayMax;

    public void OnEnable()
    {
        StartCoroutine(Walk());
    }

    private IEnumerator Walk()
    {
        while (true)
        {
            Vector2 circlePoint = Random.insideUnitCircle * walkDistance;
            Vector3 objectif = transform.position + (Vector3)circlePoint;

            Coroutine coroutine = StartCoroutine(WalkTo(objectif));
            _resourceProducterAnimator.SetBool("moving", true);

            int delay = Random.Range(betweenDelayMin, betweenDelayMax);
            yield return new WaitForSeconds(delay);

            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                _resourceProducterAnimator.SetBool("moving", false);
            }
        }
    }

    private IEnumerator WalkTo(Vector3 endPosition)
    {
        /* Flip sprite towards moving direction. */
        spriteRenderer.flipX = transform.position.x > endPosition.x;

        Vector3 startPosition = transform.position;
        float t;

        /* Smooth movement over time. */
        for (t = 0; t <= 1.0; t += Time.deltaTime * walkSpeed)
        {
            rigidbody2D.MovePosition(Vector3.Lerp(startPosition, endPosition, t));
            yield return new WaitForFixedUpdate();
        }

        _resourceProducterAnimator.SetBool("moving", false);
    }

    public void Stop()
    {
        StopAllCoroutines();
    }

    private void Update()
    {
        if (TryGetComponent<Rigidbody2D>(out Rigidbody2D rigidbody2D))
        {
            rigidbody2D.velocity = Vector2.zero;
        }
    }
}