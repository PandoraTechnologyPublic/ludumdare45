using System;
using System.Collections.Generic;
using Pandora.Scripts;
using Pandora.Scripts.Utilities;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora
{
    public class PlayerInventoryComponent : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _heldResourceSprite;

        private ResourceType _currentCarriedResource;
        public ResourceProducer _currentCarriedProducer;
        
        private ResourceDataService _resourceDataService;
        private Vector3 _savedHeldSpritePosition;

        private void Awake()
        {
            _heldResourceSprite.sprite = null;
            _savedHeldSpritePosition = _heldResourceSprite.transform.localPosition;
        }

        public void Initialise(ResourceDataService resourceDataService)
        {
            _resourceDataService = resourceDataService;
        }

        public ResourceType GetCarriedResource()
        {
            return _currentCarriedResource;
        }

        public void SetCarriedResource(ResourceType resourceType)
        {
            AchievementUtilities.UpdateAchievement(resourceType);
            _currentCarriedResource = resourceType;
        }

        private void Update()
        {
            if (_currentCarriedResource != ResourceType.NONE)
            {
                if (_heldResourceSprite.sprite == null)
                {
                    ResourceData resourceData = _resourceDataService.GetResourceData(_currentCarriedResource);
                    _heldResourceSprite.sprite = resourceData._sprite;
                    _heldResourceSprite.transform.localPosition = _savedHeldSpritePosition + new Vector3(0, resourceData._carrySpriteVerticalOffset, 0);
                }
            }
            else
            {
                _heldResourceSprite.sprite = null;
            }
        }
    }
}