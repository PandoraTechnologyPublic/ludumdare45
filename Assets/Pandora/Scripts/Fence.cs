using System;
using UnityEngine;

namespace Pandora.Scripts
{
    public class Fence : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _fenceContentSpriteRenderer;
        [SerializeField] private SpriteRenderer _collectableProductSpriteRenderer;

        private ResourceType _resourceToProduce;
        private ResourceType _fenceContent;
        private float _resourceProductionTime = 10.0f;
        
        private ResourceDataService _resourceDataService;
        private float _elapsedProductionTime;

        public void Initialise(ResourceDataService resourceDataService)
        {
            _resourceDataService = resourceDataService;
            _collectableProductSpriteRenderer.sprite = null;
        }
        
        private void Update()
        {
            if (_fenceContent != ResourceType.NONE)
            {
                _elapsedProductionTime += Time.deltaTime;
                
                if (_elapsedProductionTime >= _resourceProductionTime)
                {
                    ResourceData resourceData = _resourceDataService.GetResourceData(_resourceToProduce);
                    _collectableProductSpriteRenderer.sprite = resourceData._sprite;
                }
                else
                {
                    _collectableProductSpriteRenderer.sprite = null;
                }
                
                if (_fenceContentSpriteRenderer.sprite == null)
                {
                    ResourceData resourceData = _resourceDataService.GetResourceData(_fenceContent);
                    _fenceContentSpriteRenderer.sprite = resourceData._sprite;
                }
            }
            else
            {
                _fenceContentSpriteRenderer.sprite = null;
            }
        }

        public bool IsEmpty()
        {
            return _fenceContent == ResourceType.NONE;
        }

        public bool IsCollectable()
        {
            return _resourceToProduce != ResourceType.NONE && _elapsedProductionTime >= _resourceProductionTime;
        }
        
        public ResourceType CollectResource()
        {
            ResourceType producedResource = ResourceType.NONE;
            
            if (_elapsedProductionTime >= _resourceProductionTime)
            {
                _elapsedProductionTime = 0;
                producedResource = _resourceToProduce;
            }

            return producedResource;
        }

        public void SetContent(ResourceType resourceType)
        {
            _fenceContent = resourceType;
            ResourceData resourceData = _resourceDataService.GetResourceData(resourceType);
            ResourceProducer resourceProducerPrefab = resourceData._prefab.GetComponent<ResourceProducer>();
            _resourceToProduce = resourceProducerPrefab.GetResourceType();
            _resourceProductionTime = resourceProducerPrefab.GetResourceProductionTime() / 2.0f;
            _elapsedProductionTime = 0;
        }

        public ResourceType GetFenceContent()
        {
            return _fenceContent;
        }

        public void Clear()
        {
            _fenceContent = ResourceType.NONE;
        }
    }
}