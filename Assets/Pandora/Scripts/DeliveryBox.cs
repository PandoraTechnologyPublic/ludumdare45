using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.Scripts
{
    public class DeliveryBox : MonoBehaviour
    {
        [SerializeField] public AudioSource audioSource;

        private PlayerDataService _playerDataService;
        private ResourceDataService _resourceDataService;

        public void Initialise(PlayerDataService playerDataService, ResourceDataService resourceDataService)
        {
            _playerDataService = playerDataService;
            _resourceDataService = resourceDataService;
        }

        public void OnInteract(Resource deliveredResource)
        {
            _playerDataService.AddMoney(_resourceDataService.GetResourceData(deliveredResource._resourceType)._cost);
        }
    }
}