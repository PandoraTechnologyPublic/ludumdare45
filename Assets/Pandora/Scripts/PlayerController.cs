using Pandora.Scripts;
using UnityEngine;

namespace Pandora
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float _speed;

        [SerializeField] private AudioSource audioSource;

        private Rigidbody2D _rigidBody;

        private Vector2Int _orientation = new Vector2Int(1, 0);

        private GameStateService _gameStateService;
        private InteractionController _interactionController;
        private SpriteRenderer[] _spriteRenderers;
        private Animator _anim;

        private PlayerInventoryComponent _playerInventoryComponent;
        private static readonly int MOVING = Animator.StringToHash("moving");
        private static readonly int HOLDING = Animator.StringToHash("holding");

        public bool touchControls;
        private Vector2
            touchDirection,
            //touchOrigin,
            touchMin,
            touchMax;
        private Rect pad;

        private void Awake()
        {
            _rigidBody = GetComponent<Rigidbody2D>();
            _interactionController = GetComponent<InteractionController>();
            _spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
            _anim = GetComponentInChildren<Animator>();
            _playerInventoryComponent = GetComponent<PlayerInventoryComponent>();
            touchMin = new Vector2(0, 0.23f * Screen.width * 1.777777f + (Screen.height - Screen.width * 1.777777f));
            touchMax = new Vector2(0.465f * Screen.width, 0.48f * Screen.width * 1.777777f + (Screen.height - Screen.width * 1.777777f));
            //touchOrigin = new Vector2(0.2f * Screen.width, 0.38f * Screen.height);
            pad = new Rect(touchMin, touchMax - touchMin);
        }

        private void FixedUpdate()
        {
            if (_gameStateService != null && _gameStateService.IsCurrentState<IngameState>())
            {
                float horizontalAxis = Input.GetAxisRaw("Horizontal");
                float verticalAxis = Input.GetAxisRaw("Vertical");

                if (touchControls)
                {
                    foreach (Touch touch in Input.touches)
                    {
                        if (pad.Contains(touch.position))
                        {
                            touchDirection = touch.position - pad.center;// touchOrigin;
                        }
                    }
                    if (Application.isEditor && Input.GetMouseButton(0) && pad.Contains(new Vector2(Input.mousePosition.x, Input.mousePosition.y)))
                    {
                        touchDirection = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - pad.center;
                    }
                }

                Vector2 direction = new Vector2(horizontalAxis, verticalAxis) + touchDirection;
                touchDirection = Vector2.zero;

                direction.Normalize();

                if (direction.sqrMagnitude > 0.2f)
                {
                    _orientation = new Vector2Int((int) direction.x, (int) direction.y);
                    _interactionController.UpdateColliderOrientation(_orientation);
                    _rigidBody.MovePosition(_rigidBody.position + Time.deltaTime * _speed * direction);
                    _anim.SetBool(MOVING, true);
                }
                else
                {
                    _anim.SetBool(MOVING, false);
                }

                if (direction.x != 0)
                {
                    foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
                    {
                        spriteRenderer.flipX = (direction.x < 0);
                    }
                }
                _anim.SetBool(HOLDING, _playerInventoryComponent.GetCarriedResource() != ResourceType.NONE);
            }
        }

        public void Initialise(GameStateService gameStateService)
        {
            _gameStateService = gameStateService;
        }

        public Vector3Int GetOrientation()
        {
            return new Vector3Int(_orientation.x, _orientation.y, 0);
        }
        void OnGUI()
        {
            if (touchControls)
            {
                //if (GUI.RepeatButton(new Rect(touchMin,touchMax-touchMin), ""))
                //{
                //    touchDirection = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - touchOrigin;
                //}
                //if (GUI.RepeatButton(new Rect(30, 530, 50, 50), ""))
                //{
                //    touchDirection += new Vector2(-1, 0);
                //}
                //if (GUI.RepeatButton(new Rect(80, 480, 50, 50), ""))
                //{
                //    touchDirection += new Vector2(0, 1);
                //}
                //if (GUI.RepeatButton(new Rect(80, 580, 50, 50), ""))
                //{
                //    touchDirection += new Vector2(0, -1);
                //}
            }
        }
    }
   
}
