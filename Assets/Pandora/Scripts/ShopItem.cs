using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Pandora.Scripts
{
    public class ShopItem : MonoBehaviour
    {
        public ResourceType _resourceType;
        public TMP_Text _priceText;
        public SpriteRenderer _itemSprite;
        
        private ResourceDataService _resourceDataService;
        private Coroutine _delayPriceUpdateCoroutine;

        private void Awake()
        {
            _priceText.SetText("NOT_INITIALISED");
        }
        
        public void Initialise(ResourceDataService resourceDataService)
        {
            _resourceDataService = resourceDataService;
            _priceText.SetText(_resourceDataService.GetResourceData(_resourceType)._cost.ToString());
        }

        public void ShowNotEnoughMoneyText()
        {
            if (_delayPriceUpdateCoroutine != null)
            {
                StopCoroutine(_delayPriceUpdateCoroutine);
            }
            _priceText.SetText("NOT ENOUGH MONEY");
            _delayPriceUpdateCoroutine = StartCoroutine(DelayPriceUpdate());
        }

        private IEnumerator DelayPriceUpdate()
        {
            yield return new WaitForSeconds(1.0f);
            _priceText.SetText(_resourceDataService.GetResourceData(_resourceType)._cost.ToString());
        }
    }
}