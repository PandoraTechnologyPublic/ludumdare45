using System;
using UnityEngine;

namespace Pandora.Scripts
{
    public class ShopExit : MonoBehaviour
    {
        [SerializeField] private Transform _shopExitTransform;

        [SerializeField] private AudioSource audioSource;

        private void OnCollisionEnter2D(Collision2D other)
        {
            other.transform.position = _shopExitTransform.position;
            audioSource.Play();
        }
    }
}