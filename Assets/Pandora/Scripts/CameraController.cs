using System;
using UnityEngine;

namespace Pandora.Scripts
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Transform _target;

        private void Update()
        {
            transform.position = _target.position + Vector3.back;
        }
    }
}