﻿using System;

/**
 * Binary tree in which the key of any node is greater than or equal to its parent's key.
 */
public class Heap<T> where T : IHeapItem<T>
{
    /**
     * Items contained in this Heap.
     */
    private T[] items;

    /**
     * Number of items in this Heap.
     */
    public int Count { get; private set; }

    /**
     * Creates a new Heap with the size specified.
     */
    public Heap(int maxHeapSize)
    {
        items = new T[maxHeapSize];
    }

    /**
     * Returns whether this Heap contains items.
     */
    public bool Any()
    {
        return Count > 0;
    }

    /**
     * Returns whether this Heap contains the item specified.
     */
    public bool Contains(T item)
    {
        return Equals(items[item.HeapIndex], item);
    }

    /**
     * Adds the item specified to this Heap.
     */
    public void Add(T item)
    {
        item.HeapIndex = Count;
        items[Count] = item;

        SortUp(item);
        Count++;
    }

    /**
     * Removes and returns the root of this Heap.
     */
    public T RemoveFirst()
    {
        T root = items[0];
        Count--;

        items[0] = items[Count];
        items[0].HeapIndex = 0;
        SortDown(items[0]);

        return root;
    }

    /**
     * Rearrange this Heap after the key of the item specified changed, such as to keep this Heap ordered.
     */
    public void UpdateItem(T item)
    {
        SortUp(item);
        SortDown(item);
    }

    /**
     * Moves the item specified up in the tree as long as its parent has a greater key;
     */
    private void SortUp(T item)
    {
        int parentIndex = (item.HeapIndex - 1) / 2;

        while (true)
        {
            T parentItem = items[parentIndex];

            if (item.CompareTo(parentItem) > 0)
            {
                Swap(item, parentItem);
            }
            else
            {
                break;
            }

            parentIndex = (item.HeapIndex - 1) / 2;
        }
    }

    /**
     * Moves the item specified down in the tree as long as one of its child nodes has a smaller key.
     */
    private void SortDown(T item)
    {
        while (true)
        {
            int childIndexLeft = item.HeapIndex * 2 + 1;
            int childIndexRight = item.HeapIndex * 2 + 2;

            if (childIndexLeft < Count)
            {
                int swapIndex = childIndexLeft;

                if (childIndexRight < Count)
                {
                    if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
                    {
                        swapIndex = childIndexRight;
                    }
                }

                if (item.CompareTo(items[swapIndex]) < 0)
                {
                    Swap(item, items[swapIndex]);
                }
                else
                {
                    return;
                }

            }
            else
            {
                return;
            }

        }
    }

    /**
     * Exchanges the indexes of the two items specified.
     */
    private void Swap(T itemA, T itemB)
    {
        items[itemA.HeapIndex] = itemB;
        items[itemB.HeapIndex] = itemA;

        int itemAIndex = itemA.HeapIndex;
        itemA.HeapIndex = itemB.HeapIndex;
        itemB.HeapIndex = itemAIndex;
    }
}

/**
 * Interface required for objects to be sorted in a Heap.
 */
public interface IHeapItem<T> : IComparable<T>
{
    int HeapIndex
    {
        get;
        set;
    }
}