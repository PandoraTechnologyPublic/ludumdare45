﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

[Serializable]
public class TileSpawn
{
    /**
     * Field Tile.
     */
    [SerializeField] public TileBase tile;

    /**
     * A single Tile to spawn in each field.
     */
    [SerializeField] public TileBase item;

    /**
     * Number of fields.
     */
    [SerializeField] public int count;

    /**
     * Number of Tiles per field.
     */
    [SerializeField] public int size;
}
