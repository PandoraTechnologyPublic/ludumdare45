﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

/**
 * Generates organic shapes of tiles on a Tilemap.
 */
public class CellularAutomaton : MonoBehaviour
{
    /**
     * Tilemap this CellularAutomaton operates on.
     */
    [SerializeField] private Tilemap tilemap;

    /**
     * Tile walkable cells are made of.
     */
    [SerializeField] private TileBase grassTile;

    /**
     * Tile unwalkable cells are made of.
     */
    [SerializeField] private TileBase waterTile;

    /**
     * Size of the map on both axis.
     */
    [SerializeField] private int size;

    /**
     * Width of the sea surrounding the map.
     */
    [SerializeField] private int borderSize;

    /**
     * Proportion of walkable cells.
     */
    [SerializeField] [Range(0, 1)] private float fillPercentage;

    /**
     * Number of surrounding floors necessary to create a new floor tile.
     */
    [SerializeField] [Range(0, 8)] private int birthLimit;

    /**
     * Number of surrounding floors under which a floor tile is removed.
     */
    [SerializeField] [Range(0, 8)] private int deathLimit;

    /**
     * Number of smoothing passes.
     */
    [SerializeField] [Range(0, 10)] private int smoothness;

    /**
     * Objects that require grass around them.
     */
    [SerializeField] private GrassRadius[] grassRadius;

    /**
     * Fill the map randomly.
     */
    public void FillRandomly()
    {
        /* Set tilemap bounds. */
        tilemap.ClearAllTiles();
        tilemap.SetTile(new Vector3Int(size - 1, size - 1, 0), grassTile);

        /* Fill tilemap with random tiles. */
        tilemap.ForEach(cell => tilemap.SetTile(cell, RandomTile()));
    }

    /**
     * Returns a grass tile or a water tile.
     */
    private TileBase RandomTile()
    {
        return Random.value > fillPercentage ? waterTile : grassTile;
    }

    /**
     * Smooth all tiles based on their neighborhood. Repeats 'smoothness' times.
     */
    public void Smooth()
    {
        foreach (GrassRadius radius in grassRadius)
        {
            radius.region = new Radius(tilemap, tilemap.WorldToCell(radius.transform.position), radius.radius);
        }

        for (int i = 0; i < smoothness; i++)
        {
            SmoothOnce();
        }
    }

    /**
     * Smooth all tiles based on their neighborhood to create organic shapes.
     */
    private void SmoothOnce()
    {
        List<TileBase> smoothedTilemap = new List<TileBase>();

        foreach (Vector3Int position in tilemap.AllPositions())
        {
            if (IsGrassRequired(position))
            {
                smoothedTilemap.Add(grassTile);
                continue;
            }

            int surroundingFloors = CountNeighbors(position);

            if (tilemap.GetTile(position) == grassTile)
            {
                smoothedTilemap.Add(surroundingFloors < deathLimit ? waterTile : grassTile);
            }

            else
            {
                smoothedTilemap.Add(surroundingFloors >= birthLimit ? grassTile : waterTile);
            }
        }

        tilemap.SetTilesBlock(tilemap.cellBounds, smoothedTilemap.ToArray());
    }

    /**
     *
     */
    private bool IsGrassRequired(Vector3Int position)
    {
        foreach (GrassRadius radius in grassRadius)
        {
            if (radius.region.Contains(position))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the number of grass tiles around the cell specified.
     */
    private int CountNeighbors(Vector3Int cell)
    {
        return tilemap.Get8Neighbors(cell).Count(neighbor => tilemap.GetTile(neighbor) == grassTile);
    }

    /**
     * Encloses the map with water.
     */
    public void OutlineWithWater()
    {
        /* Extend the Tilemap bounds in each direction. */
        Vector3Int lowerCorner = new Vector3Int(tilemap.cellBounds.xMin - borderSize, tilemap.cellBounds.yMin - borderSize, 0);
        tilemap.SetTile(lowerCorner, waterTile);

        Vector3Int higherCorner = new Vector3Int(tilemap.cellBounds.xMax + borderSize - 1, tilemap.cellBounds.yMax + borderSize - 1, 0);
        tilemap.SetTile(higherCorner, waterTile);

        /* Fill border with water. */
        tilemap.Where(tilemap.IsEmpty).ForEach(cell => tilemap.SetTile(cell, waterTile));
    }
}
