﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
 * Represents an area of connected tiles in a Tilemap.
 */
public class Region
{
    /**
     * Tilemap this Region is part of.
     */
    public Tilemap tilemap;

    /**
     * Tiles in this Region.
     */
    public List<Vector3Int> tiles;

    /**
     * HashSet of the tiles in this Region used for checking containment in O(1).
     */
    private HashSet<Vector3Int> tilesHashSet;

    /**
     * Tiles in this Region with at least 1 orthogonal neighbor that is not part of the same Region.
     */
    public List<Vector3Int> edgeTiles;

    /**
     * Number of tiles in this Region.
     */
    public int Count => tiles.Count;

    /**
     * Terrain type this Region is made of.
     */
    public TileBase TileType => tilemap.GetTile(tiles[0]);

    /**
     * Creates a new Region starting from the position specified.
     */
    public Region(Tilemap tilemap, Vector3Int position)
    {
        this.tilemap = tilemap;

        tiles = FloodFill(position);
        tilesHashSet = new HashSet<Vector3Int>(tiles);
        edgeTiles = tiles.FindAll(IsEdge);
    }

    /**
     * Returns whether this Region contains the cell specified.
     */
    public bool Contains(Vector3Int position)
    {
        return tilesHashSet.Contains(position);
    }

    /**
     * Replaces all tiles in this Region by the tile specified.
     */
    public void Fill(TileBase tile)
    {
        tilemap.FloodFill(tiles[0], tile);
    }

    /**
     * Returns all identical tiles connected to the cell specified.
     */
    private List<Vector3Int> FloodFill(Vector3Int position)
    {
        List<Vector3Int> region = new List<Vector3Int>();
        TileBase tileType = tilemap.GetTile(position);

        bool[,] isExplored = new bool[tilemap.size.x, tilemap.size.y];

        Queue<Vector3Int> queue = new Queue<Vector3Int>();
        queue.Enqueue(position);
        isExplored[position.x, position.y] = true;

        /* While some connected cells have not been explored. */
        while (queue.Count > 0)
        {
            /* Explore next connected tile. */
            Vector3Int cell = queue.Dequeue();
            region.Add(cell);

            /* Add neighbors to the queue. */
            foreach (Vector3Int neighbor in tilemap.Get4Neighbors(cell))
            {
                /* Ignore neighbors with a different tile. */
                if (tilemap.GetTile(neighbor) != tileType)
                {
                    continue;
                }

                /* Ignore neighbors that were already explored. */
                if (isExplored[neighbor.x, neighbor.y])
                {
                    continue;
                }

                queue.Enqueue(neighbor);
                isExplored[neighbor.x, neighbor.y] = true;
            }
        }

        return region;
    }

    /**
     * Returns whether the cell specified has at least 1 orthogonal neighbor that is not part of this Region.
     */
    private bool IsEdge(Vector3Int position)
    {
        IEnumerable<Vector3Int> neighbors = tilemap.Get4Neighbors(position);

        return neighbors.Count() < 4 || neighbors.Any(neighbor => !Contains(neighbor));
    }

    /**
     * Returns whether this Region touches the Tilemap's bounds.
     */
    public bool IsEdge()
    {
        return edgeTiles.Any(edge => tilemap.Get4Neighbors(edge).Count() < 4);
    }
}
