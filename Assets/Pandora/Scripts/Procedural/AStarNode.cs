﻿using UnityEngine;

/**
 * Represents a cell in A* (pathfinding algorithm).
 */
public class AStarNode : IHeapItem<AStarNode>
{
    /**
     * Position of this AStarNode in the Tilemap.
     */
    public Vector3Int position;

    /**
     * Distance from starting node, and distance from destination node in A*.
     */
    public int gCost, hCost;

    /**
     * The node this AStarNode was reached from during A*.
     */
    public AStarNode parent;

    /**
     * Index of this AStarNode in the Heap.
     */
    public int HeapIndex { get; set; }

    /**
     * Creates a new AStarNode with the position specified.
     */
    public AStarNode(Vector3Int position)
    {
        this.position = position;
    }

    /**
     * Length of the shortest path going from start to destination through this AStarNode.
     */
    public int FCost => gCost + hCost;

    /**
     * Returns an integer that indicates whether this AStarNode precedes or follows
     * the AStarNode specified in the exploration order.
     */
    public int CompareTo(AStarNode other)
    {
        /* Compare fCost first. */
        int result = other.FCost.CompareTo(FCost);

        /* If fCost are equal, compare hCost. */
        if (result == 0)
        {
            result = other.hCost.CompareTo(hCost);
        }

        /* If both are equal, return one at random. */
        if (result == 0)
        {
            result = Random.value > 0.5 ? -1 : 1;
        }

        return result;
    }
}
