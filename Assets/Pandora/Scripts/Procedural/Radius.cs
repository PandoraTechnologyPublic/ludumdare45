﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
 * Represents an area of connected tiles in a Tilemap.
 */
public class Radius
{
    /**
     * Tilemap this Radius is part of.
     */
    public Tilemap tilemap;

    /**
     * Tiles in this Radius.
     */
    public List<Vector3Int> tiles;

    /**
     * HashSet of the tiles in this Radius used for checking containment in O(1).
     */
    private HashSet<Vector3Int> tilesHashSet;

    /**
     * Number of tiles in this Radius.
     */
    public int Count => tiles.Count;

    /**
     * Creates a new Radius centered on the specified position.
     */
    public Radius(Tilemap tilemap, Vector3Int position, int radius)
    {
        this.tilemap = tilemap;

        tiles = GetRegion(position, radius);
        tilesHashSet = new HashSet<Vector3Int>(tiles);
    }

    /**
     * Returns whether this Region contains the cell specified.
     */
    public bool Contains(Vector3Int position)
    {
        return tilesHashSet.Contains(position);
    }

    /**
     * Replaces all tiles in this Region by the tile specified.
     */
    public void Fill(TileBase tile)
    {
        tilemap.FloodFill(tiles[0], tile);
    }

    /**
     * Returns all identical tiles connected to the cell specified.
     */
    private List<Vector3Int> GetRegion(Vector3Int position, int radius)
    {
        List<Vector3Int> region = new List<Vector3Int>();
        bool[,] isExplored = new bool[tilemap.size.x, tilemap.size.y];

        Queue<Vector3Int> queue = new Queue<Vector3Int>();
        queue.Enqueue(position);
        isExplored[position.x, position.y] = true;

        /* While some connected cells have not been explored. */
        while (queue.Count > 0)
        {
            /* Explore next connected tile. */
            Vector3Int cell = queue.Dequeue();
            region.Add(cell);

            /* Add neighbors to the queue. */
            foreach (Vector3Int neighbor in tilemap.Get4Neighbors(cell))
            {
                /* Ignore neighbors that were already explored. */
                if (isExplored[neighbor.x, neighbor.y])
                {
                    continue;
                }

                /* Ignore neighbors that are outside of the radius specified. */
                if (PathFinding.Distance(position, neighbor) > radius)
                {
                    continue;
                }

                queue.Enqueue(neighbor);
                isExplored[neighbor.x, neighbor.y] = true;
            }
        }

        return region;
    }
}
