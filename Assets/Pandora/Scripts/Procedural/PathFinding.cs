﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
 * Component for finding the shortest path between two cells in a Tilemap.
 */
public class PathFinding : MonoBehaviour
{
    /**
     * Tiles this PathFinding can walk on.
     */
    [SerializeField] private List<TileBase> walkableTiles;

    /**
     * Objects this Pathfinding cannot go through.
     */
    [SerializeField] private Transform[] unwalkableObjets;

    /**
     * Closest distance this Pathfinding can walk along unwalkableObjects.
     */
    [SerializeField] private int distanceFromObjects;

    /**
     * Tilemap this PathFinding operates on.
     */
    private Tilemap tilemap;

    /**
     * Array of nodes representing the Tilemap cells.
     */
    private AStarNode[,] nodes;

    /**
     * Nodes that were explored by the last pathfinding search.
     */
    private List<AStarNode> closedNodes;

    /**
     * Initializes references.
     */
    private void Awake()
    {
        tilemap = tilemap ?? FindObjectOfType<Tilemap>();
    }

    /**
     * Initializes this PathFinding.
     */
    public void Initialize()
    {
        Awake();

        Vector3Int tilemapSize = tilemap.cellBounds.size;
        nodes = new AStarNode[tilemapSize.x, tilemapSize.y];

        foreach (Vector3Int position in tilemap.AllPositions())
        {
            Vector3Int index = WorldToIndex(position);

            nodes[index.x, index.y] = new AStarNode(position);
        }
    }

    /**
     * Converts a cell position from [Tilemap.cellBounds.min; Tilemap.cellBounds.max] to [0; Tilemap.cellBounds.size]
     */
    private Vector3Int WorldToIndex(Vector3Int position)
    {
        return position - tilemap.cellBounds.min;
    }

    /**
     * Returns the node at the position specified.
     */
    private AStarNode GetNode(Vector3Int position)
    {
        Vector3Int index = WorldToIndex(position);

        return nodes[index.x, index.y];
    }

    /**
     * Returns whether the tile specified can be occupied by a Creature.
     */
    public bool IsWalkable(TileBase tile)
    {
        return walkableTiles.Contains(tile);
    }

    /**
     * Returns whether the cell specified can be occupied by a Creature.
     */
    public bool IsWalkable(Vector3Int position)
    {
        foreach (Transform transform in unwalkableObjets)
        {
            Vector3Int cell = tilemap.WorldToCell(transform.position);

            if (transform == unwalkableObjets[0])
            {
                cell.y++;
            }

            if (Distance(cell, position) < distanceFromObjects)
            {
                return false;
            }
        }

        return IsWalkable(tilemap.GetTile(position));
    }

    /**
     * Returns the distance in cells between the two cells specified.
     */
    public static int Distance(Vector3Int a, Vector3Int b)
    {
        int deltaX = Mathf.Abs(a.x - b.x);
        int deltaY = Mathf.Abs(a.y - b.y);

        return deltaX + deltaY;
    }

    /**
     * Returns the distance in cells between the two AStarNode specified.
     */
    private int Distance(AStarNode a, AStarNode b)
    {
        return Distance(a.position, b.position);
    }

    /**
     * Returns whether a path exists from the origin specified to the destination specified.
     */
    public bool HasPath(Vector3Int origin, Vector3Int destination)
    {
        return FindPath(origin, destination) != null;
    }

    /**
     * Returns the length of the shortest path from the origin specified to the destination specified.
     */
    public int PathLength(Vector3Int origin, Vector3Int destination)
    {
        List<Vector3Int> path = FindPath(origin, destination);

        return path == null ? -1 : path.Count;
    }

    /**
     * Returns one of the shortest path from the origin specified to the destination specified.
     */
    public List<Vector3Int> FindPath(Vector3Int origin, Vector3Int destination)
    {
        /* Initialize this PathFinding if it isn't yet. */
        if (nodes == null)
        {
            Initialize();
        }

        AStarNode originNode = GetNode(origin);
        AStarNode destinationNode = GetNode(destination);

        /* Keep track of which nodes have been explored. */
        Heap<AStarNode> openNodes = new Heap<AStarNode>(tilemap.Size());
        closedNodes = new List<AStarNode>();

        /* Start by exploring the origin node. */
        originNode.hCost = Distance(origin, destination);
        openNodes.Add(originNode);

        /* Search a path while some reachable nodes are unexplored. */
        while (openNodes.Any())
        {
            /* Select the unexplored node with the lowest movement cost. */
            AStarNode currentNode = openNodes.RemoveFirst();

            /* Mark it as explored. */
            closedNodes.Add(currentNode);

            /* If a path to the destination is found, return it. */
            if (currentNode == destinationNode)
            {
                return RetracePath(originNode, destinationNode);
            }

            /* Else update movement costs of neighbors. */
            foreach (Vector3Int neighborPosition in tilemap.Get4Neighbors(currentNode.position))
            {
                AStarNode neighbor = GetNode(neighborPosition);

                /* Ignore neighbors that were already explored. */
                if (closedNodes.Contains(neighbor))
                {
                    continue;
                }

                /* Ignore neighbors that are not walkable. */
                if (!IsWalkable(neighborPosition))
                {
                    continue;
                }

                /* Calculate cost of going from the origin to the neighbor through the current node. */
                int newMovementCost = currentNode.gCost + Distance(currentNode, neighbor);

                /* If that neighbor has not been visited yet, or a shorter path to that neighbor was found. */
                if (!openNodes.Contains(neighbor) || newMovementCost < neighbor.gCost)
                {
                    /* Update movement costs of that neighbor. */
                    neighbor.gCost = newMovementCost;
                    neighbor.hCost = Distance(neighbor, destinationNode);
                    neighbor.parent = currentNode;

                    /* Add that neighbor to the heap if not contained in it yet. */
                    if (!openNodes.Contains(neighbor))
                    {
                        openNodes.Add(neighbor);
                    }
                    /* Else update the neighbor's position in the heap based on its new costs. */
                    else
                    {
                        openNodes.UpdateItem(neighbor);
                    }
                }
            }
        }

        /* Return null if no path to the destination exist. */
        return null;
    }

    /**
     * Returns a path from the origin to the node closest to the destination among reachable nodes.
     */
    public List<Vector3Int> FindClosestPath(Vector3Int origin, Vector3Int destination)
    {
        List<Vector3Int> path = FindPath(origin, destination);

        /* If a path to the destination exists, return it. */
        if (path != null)
        {
            return path;
        }

        /* Else return a path to the node closest to the destination. */
        AStarNode closestToDestination = closedNodes.OrderBy(a => a.hCost).First();

        return RetracePath(GetNode(origin), closestToDestination);
    }

    private List<Vector3Int> RetracePath(AStarNode origin, AStarNode destination)
    {
        List<Vector3Int> path = new List<Vector3Int>();
        AStarNode currentNode = destination;

        while (currentNode != origin)
        {
            path.Add(currentNode.position);
            currentNode = currentNode.parent;
        }

        path.Reverse();
        return path;
    }
}