﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
 * Component for placing particular Tiles and GameObjects on a Tilemap.
 */
public class ObjectScatterer : MonoBehaviour
{
    /**
     * Tilemap the Tiles are placed in.
     */
    [SerializeField] private Tilemap tilemap;

    /**
     * Tile walkable cells are made of.
     */
    [SerializeField] private TileBase grassTile;

    [SerializeField] private TileBase waterTile;

    /**
     * Tiles to scatter.
     */
    [SerializeField] private TileSpawn[] tilesToSpawn;

    /**
     * Objects to scatter.
     */
    [SerializeField] private ObjectSpawn[] objectsToSpawn;

    [SerializeField] private TileBase saltTile;

    [SerializeField] private int saltCount;

    /**
     * Positions the tiles and objects.
     */
    public void ScatterObjects()
    {
        /* Shuffle all walkable tiles. */
        Queue<Vector3Int> shuffledTiles = tilemap.Where(grassTile)/*.Except(player cell)*/.Shuffle().ToQueue();

        /* Place tiles at random. */
        foreach (TileSpawn tileSpawn in tilesToSpawn)
        {
            for (int i = 0; i < tileSpawn.count; i++)
            {
                Vector3Int origin = shuffledTiles.Dequeue();

                HashSet<Vector3Int> field = new HashSet<Vector3Int> {origin};

                tilemap.SetTile(origin, tileSpawn.tile);
                int spawnedTiles = 1;

                while (spawnedTiles < tileSpawn.size)
                {
                    IEnumerable<Vector3Int> allNeighbors = field.SelectMany(cell => tilemap.Get4Neighbors(cell));
                    Vector3Int newTile = allNeighbors.Where(cell => tilemap.GetTile(cell) == grassTile).GetRandom();

                    field.Add(newTile);
                    tilemap.SetTile(newTile, tileSpawn.tile);
                    spawnedTiles++;
                }

                if (tileSpawn.item != null)
                {
                    tilemap.SetTile(field.GetRandom(), tileSpawn.item);
                }
            }
        }

        /* Place objects at random. */
        foreach (ObjectSpawn objectSpawn in objectsToSpawn)
        {
            for (int i = 0; i < objectSpawn.count; i++)
            {
                Instantiate(objectSpawn.prefab, shuffledTiles.Dequeue(), Quaternion.identity);
            }
        }
        
        /* Place SALT. */
        IEnumerable<Vector3Int> nextToSea = tilemap.Where(waterTile).SelectMany(tilemap.Get4Neighbors).Where(cell => tilemap.GetTile(cell) == grassTile).Where(NearWater);
        Queue<Vector3Int> queue = nextToSea.Shuffle().ToQueue();

        for (int i = 0; i < saltCount && queue.Any(); i++)
        {
            tilemap.SetTile(queue.Dequeue(), saltTile);
        }
    }

    private bool NearWater(Vector3Int cell)
    {
        return cell.x <= 5 || cell.x >= 95 || cell.y <= 5 || cell.y >= 95;
    }

    public void RespawnOneSalt()
    {
        Vector3Int nextToSea = tilemap.Where(waterTile).SelectMany(tilemap.Get4Neighbors).Where(cell => tilemap.GetTile(cell) == grassTile).Where(NearWater).GetRandom();

        tilemap.SetTile(nextToSea, saltTile);
    }
}
