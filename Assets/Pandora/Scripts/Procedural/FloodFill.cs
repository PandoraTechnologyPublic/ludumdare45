﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
 * Component for detecting Regions in a Tilemap and perform operations on them.
 */
public class FloodFill : MonoBehaviour
{
    /**
     * Tilemap this FloodFill operates on.
     */
    [SerializeField] private Tilemap tilemap;

    /**
     * Tile walkable cells are made of.
     */
    [SerializeField] private TileBase grassTile;

    /**
     * Tile unwalkable cells are made of.
     */
    [SerializeField] private TileBase waterTile;

    /**
     * Grass regions under this size are filled with water tiles.
     */
    [SerializeField] private int smallestGrassRegions;

    /**
     * Water regions under this size are filled with grass tiles.
     */
    [SerializeField] private int smallestWaterRegions;

    /**
     * Returns all Regions in the Tilemap made of the tile specified.
     */
    public IEnumerable<Region> FindAllRegions(TileBase tileType)
    {
        List<Region> regions = new List<Region>();
        bool[,] isInARegion = new bool[tilemap.size.x, tilemap.size.y];

        foreach (Vector3Int position in tilemap.AllPositions())
        {
            /* Ignore other tiles. */
            if (tilemap.GetTile(position) != tileType)
            {
                continue;
            }

            /* Ignore cells that are already part of a region. */
            if (isInARegion[position.x, position.y])
            {
                continue;
            }

            Region newRegion = new Region(tilemap, position);
            regions.Add(newRegion);

            /* Mark all cells in the region. */
            foreach (Vector3Int tile in newRegion.tiles)
            {
                isInARegion[tile.x, tile.y] = true;
            }
        }

        return regions;
    }

    /**
     * Removes regions that are too small from the Tilemap.
     */
    public void EraseSmallRegions()
    {
        EraseSmallRegions(grassTile, waterTile, smallestGrassRegions);
        EraseSmallRegions(waterTile, grassTile, smallestWaterRegions);
    }

    /**
     * Removes regions made of the tile specified if they are smaller than the size specified.
     */
    private void EraseSmallRegions(TileBase tileToReplace, TileBase tileToFill, int smallestSize)
    {
        IEnumerable<Region> regions = FindAllRegions(tileToReplace);

        foreach (Region region in regions)
        {
            /* Skip regions that are too large. */
            if (region.Count >= smallestSize)
            {
                continue;
            }

            /* Skip water regions at the edges of the tilemap. */
            if (tileToReplace == waterTile && region.IsEdge())
            {
                continue;
            }

            region.Fill(tileToFill);
        }
    }
}
