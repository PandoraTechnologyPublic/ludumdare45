﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

/**
 * Generates random maps.
 */
public class ProceduralMap : MonoBehaviour
{
    /**
     * Seed for the pseudorandom number generator set via the inspector.
     */
    [SerializeField] private string seed;

    /**
     * Tilemap the world is built on.
     */
    [SerializeField] private Tilemap tilemap;

    /**
     * Component to generate the map's layout.
     */
    [SerializeField] private CellularAutomaton automaton;

    /**
     * Component to detect Regions in the tilemap.
     */
    [SerializeField] private FloodFill floodFill;

    /**
     * Component to scatter obstacles and resources.
     */
    [SerializeField] private ObjectScatterer objectScatterer;

    /**
     * Component to draw roads.
     */
    [SerializeField] private ProceduralRoads proceduralRoads;

    /**
     * Initializes the seed and generates a map.
     */
    private void Awake()
    {
        /* If a seed is not provided, generate one. */
        if (seed.IsEmpty())
        {
            seed = DateTime.Now.ToOADate().ToString();
        }

        /* Initialize RNG. */
        Random.InitState(seed.GetHashCode());
        
        /* Copy seed to clipboard for easy sharing. */
        //Clipboard.SetText(seed);
        
        /* Fill the map randomly. */
        automaton.FillRandomly();

        /* Smooth tiles based on their neighborhood to create shapes. */
        automaton.Smooth();

        /* Remove regions of tiles that are too small. */
        floodFill.EraseSmallRegions();
        
        /* Enclose the map with water. */
        automaton.OutlineWithWater();

        /* Place obstacles and resources. */
        objectScatterer.ScatterObjects();

        /* Draw roads. */
        proceduralRoads.DrawRoads();
    }
}
