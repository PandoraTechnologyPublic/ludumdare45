﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

/**
 * Component for updating the map periodically.
 */
public class WorldTick : MonoBehaviour
{
    /**
     * Tilemap to update.
     */
    [SerializeField] private Tilemap tilemap;

    /**
     * Component for detecting Regions of tiles.
     */
    [SerializeField] private FloodFill floodFill;

    /**
     * Tile on which resources can grow.
     */
    [SerializeField] private TileBase dirtTile;

    /**
     * Tiles to reproduce.
     */
    [SerializeField] private TileBase[] plantTiles;

    /**
     * Number of updates performed immediately.
     */
    [SerializeField] private int initialTicks;

    /**
     * Seconds between each update.
     */
    [SerializeField] private int tickSpeed;

    /**
     * Starts updating the map periodically.
     */
    private void Start()
    {
        for (int i = 0; i < initialTicks; i++)
        {
            Tick();
        }

        InvokeRepeating("Tick", tickSpeed, tickSpeed);
    }

    /**
     * Grows resources.
     */
    private void Tick()
    {
        foreach (TileBase plant in plantTiles)
        {
            IEnumerable<Region> plantFields = floodFill.FindAllRegions(plant);

            foreach (Region field in plantFields)
            {
                Vector3Int neighbor = field.edgeTiles.SelectMany(tilemap.Get4Neighbors).Where(CanGrow).GetRandom();

                tilemap.SetTile(neighbor, plant);
            }
        }
    }

    /**
     * Returns whether a resource tile can grow on the cell specified.
     */
    private bool CanGrow(Vector3Int cell)
    {
        return tilemap.GetTile(cell) == dirtTile;
    }
}
