﻿using System;
using UnityEngine;

[Serializable]
public class ObjectSpawn
{
    /**
     * GameObject to spawn.
     */
    [SerializeField] public GameObject prefab;

    /**
     * Number of times the object is spawned.
     */
    [SerializeField] public int count;
}
