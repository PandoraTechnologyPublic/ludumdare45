﻿using System;
using UnityEngine;

[Serializable]
public class GrassRadius
{
    [SerializeField] public Transform transform;

    [SerializeField] public int radius;

    [HideInInspector] public Radius region;
}
