﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ProceduralRoads : MonoBehaviour
{
    [SerializeField] private Tilemap tilemap;

    [SerializeField] private PathFinding pathFinding;

    [SerializeField] private TileBase roadTile;

    [SerializeField] private Transform player;

    [SerializeField] private Transform shop;

    public void DrawRoads()
    {
        Vector3 playerPosition = new Vector3(player.position.x-1, player.position.y -1, player.position.z);
        Vector3 shopPosition = new Vector3(shop.position.x-1, shop.position.y -2, shop.position.z);

        Vector3Int playerCell = tilemap.WorldToCell(playerPosition);
        Vector3Int shopCell = tilemap.WorldToCell(shopPosition);

        pathFinding.Initialize();
        List<Vector3Int> path = pathFinding.FindPath(playerCell, shopCell);

        tilemap.SetTiles(path, roadTile);
    }
}
