using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Pandora.Scripts
{
    public class InteractionController : MonoBehaviour
    {
        [SerializeField] private InteractionCollider _interactionCollider;
        [SerializeField] private Tilemap _tilemap;
        [SerializeField] private ObjectScatterer objectScatterer;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip dropSound;
        [SerializeField] private AudioClip buySound;

        private PlayerController _playerController;
        private PlayerInventoryComponent _playerInventoryComponent;
        private PlayerTileHighlighter _playerTileHighlighter;
        private ResourceDataService _resourceDataService;
        private PlayerDataService _playerDataService;

        private bool touchButtonPressed, touchControls;
        private Rect touchButton;
        private void Awake()
        {
            _playerController = GetComponent<PlayerController>();
            _playerInventoryComponent = GetComponent<PlayerInventoryComponent>();
            _playerTileHighlighter = GetComponent<PlayerTileHighlighter>();
            if (_playerController.touchControls) touchControls = true;
            touchButton = new Rect(0.6f * Screen.width, 0.25f * Screen.width * 1.777777f + (Screen.height - Screen.width * 1.777777f), 0.4f * Screen.width, 0.23f * Screen.width * 1.777777f + (Screen.height - Screen.width * 1.777777f));
        }

        public void Initialise(ResourceDataService resourceDataService, PlayerDataService playerDataService)
        {
            _resourceDataService = resourceDataService;
            _playerDataService = playerDataService;
        }

            private void Update()
        {
            if (TryGetComponent<Rigidbody2D>(out Rigidbody2D rigidbody))
            {
                rigidbody.velocity = Vector2.zero;
            }

            foreach (Touch touch in Input.touches)
            {
                if(touch.phase == TouchPhase.Began && touchButton.Contains(touch.position))
                {
                    touchButtonPressed = true;
                    
                }
            }
            if ( Application.isEditor && Input.GetMouseButtonUp(0) && touchButton.Contains(new Vector2(Input.mousePosition.x, Input.mousePosition.y)))
            {
                touchButtonPressed = true;
            }

                if (Input.GetButtonUp("Fire1") || touchButtonPressed)
            {
                touchButtonPressed = false;
                bool hasHandleCollider = false;
                
                if (!_interactionCollider.collidingItems.IsEmpty())
                {
                    GameObject collidingItem = _interactionCollider.collidingItems.Last();

                    hasHandleCollider = true;
                    
                    if (collidingItem.TryGetComponent<ShopEntrance>(out ShopEntrance shopEntrance))
                    {
                        gameObject.transform.position = shopEntrance._shopTransform.position;
                        audioSource.PlayOneShot(shopEntrance.audioClip, 0.35f);
                    }
                    else if (collidingItem.TryGetComponent<DeliveryBox>(out DeliveryBox deliveryBox))
                    {
                        HandleDeliveryBoxInteraction(deliveryBox);
                    }
                    else if (collidingItem.TryGetComponent<TransformBox>(out TransformBox transformBox))
                    {
                        ResourceType carriedResource = _playerInventoryComponent.GetCarriedResource();
                        
                        if (carriedResource == ResourceType.NONE && transformBox.GetState() == TransformBox.TransformBoxState.IDLE && transformBox.IsEmpty())
                        {
                            HandleInteractingEntity(transformBox.GetComponent<InteractingEntityComponent>(), transformBox.gameObject);
                        }
                        else
                        {
                            HandleTransformBoxInteraction(transformBox, carriedResource);
                        }
                    }
                    else if (collidingItem.TryGetComponent<Fence>(out Fence fence))
                    {
                        ResourceType carriedResource = _playerInventoryComponent.GetCarriedResource();
                        
                        if (carriedResource == ResourceType.NONE && fence.IsEmpty())
                        {
                            HandleInteractingEntity(fence.GetComponent<InteractingEntityComponent>(), fence.gameObject);
                        }
                        else
                        {
                            HandleFenceInteraction(fence, carriedResource);
                        }
                    }
                    else if (collidingItem.TryGetComponent<InteractingEntityComponent>(out InteractingEntityComponent interactingEntityComponent))
                    {
                        if (collidingItem.TryGetComponent<Wander>(out Wander wander))
                        {
                            wander.Stop();
                        }

                        if (TryGetComponent<Rigidbody2D>(out Rigidbody2D rigidbody2D))
                        {
                            rigidbody2D.velocity = Vector2.zero;
                        }

                        HandleInteractingEntity(interactingEntityComponent, collidingItem);
                    }
                    else if (collidingItem.TryGetComponent<ShopItem>(out ShopItem shopItem))
                    {
                        HandleShopItem(shopItem);
                    }
                    else
                    {
                        hasHandleCollider = false;
                    }
                }
                
                if (!hasHandleCollider)
                {
                    if (_playerInventoryComponent.GetCarriedResource() == ResourceType.NONE)
                    {
                        HandleSelectedTile();
                    }
                    else
                    {
                        HandleCarriedItem();
                    }
                }
            }
            else
            {
                if (_playerDataService != null)
                {
                    _playerDataService.SetDisplayResourceList(null);
                }

                if (!_interactionCollider.collidingItems.IsEmpty())
                {
                    GameObject collidingItem = _interactionCollider.collidingItems[0];
                    TransformBox transformBox = collidingItem.GetComponent<TransformBox>();
                    
                    if (transformBox != null)
                    {
                        transformBox.ShowInputResources(_playerDataService);
                    }
                }
            }
        }

        private void HandleCarriedItem()
        {
            ResourceType currentCarriedResource = _playerInventoryComponent.GetCarriedResource();
            ResourceData resourceData = _resourceDataService.GetResourceData(currentCarriedResource);

            if (currentCarriedResource == ResourceType.PLOWED_LAND)
            {
                Vector3Int cellPosition = _playerTileHighlighter.GetHighlitedTilePosition();
                _tilemap.SetTile(cellPosition, resourceData._tile);
                _playerInventoryComponent.SetCarriedResource(ResourceType.NONE);
            }
            else
            {
                Vector3 resourcesSpawnPosition = transform.position + _playerController.GetOrientation();

                if (_playerInventoryComponent._currentCarriedProducer != null)
                {
                    _playerInventoryComponent._currentCarriedProducer.transform.position = resourcesSpawnPosition;
                    _playerInventoryComponent._currentCarriedProducer.gameObject.SetActive(true);
                    _playerInventoryComponent._currentCarriedProducer = null;

                    audioSource.PlayOneShot(dropSound);
                }
                else
                {
                    PandoraTile tile = _playerTileHighlighter.GetHighlightedTile();

                    if (tile != null)
                    {
                        if (resourceData._isGrowable && (tile.resourceType == ResourceType.PLOWED_LAND))
                        {
                            Vector3Int cellPosition = _playerTileHighlighter.GetHighlitedTilePosition();
                            _tilemap.SetTile(cellPosition, resourceData._tile);
                            _playerTileHighlighter.UnhighlightTile();
                            _playerInventoryComponent.SetCarriedResource(ResourceType.NONE);
                        }
                    }
                    else
                    {
                        InteractingEntityComponent interactingEntityComponent = GameObject.Instantiate(resourceData._prefab, resourcesSpawnPosition, Quaternion.identity);
                        interactingEntityComponent.gameObject.GetComponent<TransformBox>()?.Initialise(_resourceDataService);
                        interactingEntityComponent.gameObject.GetComponent<Fence>()?.Initialise(_resourceDataService);

                        audioSource.PlayOneShot(dropSound);
                    }
                }

                _playerInventoryComponent.SetCarriedResource(ResourceType.NONE);
            }
        }

        private void HandleSelectedTile()
        {
            PandoraTile tile = _playerTileHighlighter.GetHighlightedTile();

            if (tile != null)
            {
                if (tile.isPlowable && _playerInventoryComponent.GetCarriedResource() == ResourceType.PLOWED_LAND)
                {
                    Vector3Int cellPosition = _playerTileHighlighter.GetHighlitedTilePosition();
                    ResourceData resourceData = _resourceDataService.GetResourceData(_playerInventoryComponent.GetCarriedResource());
                    _tilemap.SetTile(cellPosition, resourceData._tile);
                    _playerTileHighlighter.UnhighlightTile();
                }
                else if (tile.resourceType != ResourceType.NONE && _playerInventoryComponent.GetCarriedResource() == ResourceType.NONE && tile.isCollectable)
                {
                    _playerInventoryComponent.SetCarriedResource(tile.resourceType);
                    Debug.Log($"Collected {tile.resourceType}");

                    if (tile.fallbackTile != null)
                    {
                        Vector3Int cellPosition = _playerTileHighlighter.GetHighlitedTilePosition();
                        _tilemap.SetTile(cellPosition, tile.fallbackTile);
                        _playerTileHighlighter.UnhighlightTile();
                    }

                    ResourceData resourceData = _resourceDataService.GetResourceData(tile.resourceType);
                    if (resourceData.pickUpSound != null)
                    {
                        audioSource.PlayOneShot(resourceData.pickUpSound);
                    }
                }

                if (tile.resourceType == ResourceType.SALT)
                {
                    objectScatterer.RespawnOneSalt();
                }
            }
        }

        private void HandleInteractingEntity(InteractingEntityComponent interactingEntityComponent, GameObject collidingItem)
        {
            ResourceType resourceType = interactingEntityComponent._resourceType;

            if (resourceType != ResourceType.NONE && _playerInventoryComponent.GetCarriedResource() == ResourceType.NONE)
            {
                _playerInventoryComponent.SetCarriedResource(resourceType);
                Debug.Log($"Collected {resourceType}");

                ResourceProducer resourceProducer = collidingItem.GetComponent<ResourceProducer>();

                ResourceData resourceDat = _resourceDataService.GetResourceData(resourceType);
                if (resourceDat.pickUpSound != null)
                {
                    audioSource.PlayOneShot(resourceDat.pickUpSound);
                }

                if (resourceProducer != null)
                {
                    ResourceType producedResourceType = resourceProducer.CollectResource();

                    if (producedResourceType != ResourceType.NONE)
                    {
                        ResourceData resourceData = _resourceDataService.GetResourceData(producedResourceType);
                        GameObject.Instantiate(resourceData._prefab, resourceProducer.transform.position, Quaternion.identity);
                    }

                    resourceProducer.gameObject.gameObject.SetActive(false);
                    _playerInventoryComponent._currentCarriedProducer = resourceProducer;
                }
                else
                {
                    GameObject.Destroy(interactingEntityComponent.gameObject);
                }
            }
        }

        private void HandleShopItem(ShopItem shopItem)
        {
            ResourceType shopItemResourceType = shopItem._resourceType;

            if (shopItemResourceType != ResourceType.NONE && _playerInventoryComponent.GetCarriedResource() == ResourceType.NONE)
            {
                int cost = _resourceDataService.GetResourceData(shopItemResourceType)._cost;
                int playerMoney = _playerDataService.GetMoney();

                if (playerMoney >= cost)
                {
                    _playerInventoryComponent.SetCarriedResource(shopItemResourceType);
                    Debug.Log($"Buy {shopItemResourceType} - Cost : {cost}");
                    _playerDataService.PayMoney(cost);
                    audioSource.PlayOneShot(buySound);
                }
                else
                {
                    shopItem.ShowNotEnoughMoneyText();
                }
            }
        }

        private void HandleDeliveryBoxInteraction(DeliveryBox deliveryBox)
        {
            ResourceType carriedResource = _playerInventoryComponent.GetCarriedResource();
            if (carriedResource != ResourceType.NONE)
            {
                deliveryBox.OnInteract(new Resource(carriedResource));
                _playerInventoryComponent.SetCarriedResource(ResourceType.NONE);
                
                if (_playerInventoryComponent._currentCarriedProducer != null)
                {
                    GameObject.Destroy(_playerInventoryComponent._currentCarriedProducer.gameObject);
                }

                deliveryBox.audioSource.Play();
                Debug.Log($"Delivered {carriedResource} in delivery box.");
            }
        }

        private void HandleFenceInteraction(Fence fence, ResourceType carriedResource)
        {
            if (carriedResource != ResourceType.NONE && fence.IsEmpty() && _playerInventoryComponent._currentCarriedProducer != null)
            {
                fence.SetContent(carriedResource);
                _playerInventoryComponent.SetCarriedResource(ResourceType.NONE);
                GameObject.Destroy(_playerInventoryComponent._currentCarriedProducer.gameObject);
                
                Debug.Log($"Put {carriedResource} in a fence.");
            }
            else if (carriedResource == ResourceType.NONE && fence.IsCollectable())
            {
                ResourceType collectResource = fence.CollectResource();
                _playerInventoryComponent.SetCarriedResource(collectResource);
            }
            else if ((carriedResource == ResourceType.NONE) && !fence.IsCollectable())
            {
                ResourceType fenceContent = fence.GetFenceContent();
                _playerInventoryComponent.SetCarriedResource(fenceContent);
                InteractingEntityComponent interactingEntityComponent = GameObject.Instantiate(_resourceDataService.GetResourceData(fenceContent)._prefab);
                _playerInventoryComponent._currentCarriedProducer = interactingEntityComponent.GetComponent<ResourceProducer>();
                fence.Clear();
            }
        }

        private void HandleTransformBoxInteraction(TransformBox transformBox, ResourceType carriedResource)
        {
            if (carriedResource != ResourceType.NONE && transformBox.GetState() == TransformBox.TransformBoxState.IDLE)
            {
                bool wasInteractionSuccessful = transformBox.OnInteract(new Resource(carriedResource));

                if (wasInteractionSuccessful)
                {
                    _playerInventoryComponent.SetCarriedResource(ResourceType.NONE);
                    
                    if (_playerInventoryComponent._currentCarriedProducer != null)
                    {
                        GameObject.Destroy(_playerInventoryComponent._currentCarriedProducer.gameObject);
                    }
                    
                    Debug.Log($"Added {carriedResource} in transform box.");
                }
            }
            else if (carriedResource == ResourceType.NONE && transformBox.GetState() == TransformBox.TransformBoxState.ENDED_PROCESSING)
            {
                Resource resource = transformBox.OnCollect();

                if (resource != null)
                {
                    _playerInventoryComponent.SetCarriedResource(resource._resourceType);
                    Debug.Log($"Collected {resource._resourceType} from transform box.");

                    ResourceData resourceDat = _resourceDataService.GetResourceData(resource._resourceType);
                    if (resourceDat.pickUpSound != null)
                    {
                        audioSource.PlayOneShot(resourceDat.pickUpSound);
                    }
                }
            }
            else if (carriedResource == ResourceType.NONE && transformBox.GetState() == TransformBox.TransformBoxState.IDLE)
            {
                Resource resource = transformBox.RecoverItem();

                if (resource != null)
                {
                    _playerInventoryComponent.SetCarriedResource(resource._resourceType);
                    Debug.Log($"Recover {resource._resourceType} from transform box.");

                    ResourceData resourceDat = _resourceDataService.GetResourceData(resource._resourceType);
                    if (resourceDat.pickUpSound != null)
                    {
                        audioSource.PlayOneShot(resourceDat.pickUpSound);
                    }
                }
            }
        }

        public Vector3Int GetCollidingCellPosition(Tilemap tilemap)
        {
            Vector3Int playerOrientation = _playerController.GetOrientation();
            Vector3 playerPosition = transform.position + new Vector3(0, -0.5f, 0); // offset the center towards the feet

            return tilemap.WorldToCell(playerPosition + ((Vector3) playerOrientation * 0.02f)) + playerOrientation;
        }

        public void UpdateColliderOrientation(Vector2Int orientation)
        {
            int rotationAngle = 0;

            if (orientation.x == -1)
            {
                rotationAngle = 180;
            }
            else if (orientation.y == 1)
            {
                rotationAngle = 90;
            }
            else if (orientation.y == -1)
            {
                rotationAngle = -90;
            }

            _interactionCollider.transform.eulerAngles = new Vector3(0, 0, rotationAngle);
        }
    }
}